﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.LTLBuilders;
using TemporalLogicVerifier.LTLFormula;

namespace TemporalLogicVerifier.Test
{
    [TestClass]
    public class BuchiAutomatonByLTLFormulaTests
    {
        [TestMethod]
        public void Buchi_automaton_by_LTL_formula_1()
        {
            var p = new AtomicPredicate("p");
            var f = new F(p);

            var automaton = BuchiAutomatonBuilder.Build(f);

            var state0 = automaton.States[0];
            var state1 = automaton.States[1];
            var state2 = automaton.States[2];

            Assert.AreEqual(3, automaton.States.Count);
            Assert.AreEqual(0, automaton.StateValue(state0).Count());
            Assert.IsTrue(automaton.StateValue(state1).Contains(f));
            Assert.IsTrue(automaton.StateValue(state2).Contains(f));
            Assert.IsTrue(automaton.StateValue(state2).Contains(p));

            Assert.AreEqual(2, automaton.InitStates.Count);
            Assert.IsTrue(automaton.InitStates.Contains(state1));
            Assert.IsTrue(automaton.InitStates.Contains(state2));

            Assert.AreEqual(2, automaton.FinalStates.Count);
            Assert.AreEqual(state0, automaton.FinalStates[0]);
            Assert.AreEqual(state2, automaton.FinalStates[1]);

            Assert.AreEqual(1, automaton.EdgeValue(state0, state0).Count);
            Assert.AreEqual(LTLFormulaType.Empty, automaton.EdgeValue(state0, state0)[0].Type);

            Assert.AreEqual(1, automaton.EdgeValue(state1, state1).Count);
            Assert.AreEqual(LTLFormulaType.Empty, automaton.EdgeValue(state1, state1)[0].Type);

            Assert.AreEqual(1, automaton.EdgeValue(state1, state2).Count);
            Assert.AreEqual(LTLFormulaType.Empty, automaton.EdgeValue(state1, state2)[0].Type);

            Assert.AreEqual(1, automaton.EdgeValue(state2, state1).Count);
            Assert.IsTrue(automaton.EdgeValue(state2, state1).Contains(p));

            Assert.AreEqual(1, automaton.EdgeValue(state2, state2).Count);
            Assert.IsTrue(automaton.EdgeValue(state2, state2).Contains(p));

            Assert.AreEqual(1, automaton.EdgeValue(state2, state0).Count);
            Assert.IsTrue(automaton.EdgeValue(state2, state0).Contains(p));
        }

        [TestMethod]
        public void Buchi_automaton_by_LTL_formula_2()
        {
            var p = new AtomicPredicate("p");
            var f = new F(p);
            var g = new G(f);

            var automaton = BuchiAutomatonBuilder.Build(g);

            var state1 = automaton.States[0];
            var state2 = automaton.States[1];
            var state3 = automaton.States[2];
            var state4 = automaton.States[3];
            var state5 = automaton.States[4];

            Assert.AreEqual(5, automaton.States.Count);

            Assert.AreEqual(2, automaton.StateValue(state4).Count());
            Assert.AreEqual(3, automaton.StateValue(state5).Count());
            Assert.IsTrue(automaton.StateValue(state4).Contains(f));
            Assert.IsTrue(automaton.StateValue(state4).Contains(g));
            Assert.IsTrue(automaton.StateValue(state5).Contains(p));
            Assert.IsTrue(automaton.StateValue(state5).Contains(f));
            Assert.IsTrue(automaton.StateValue(state5).Contains(g));

            Assert.AreEqual(2, automaton.InitStates.Count);
            Assert.IsTrue(automaton.InitStates.Contains(state4));
            Assert.IsTrue(automaton.InitStates.Contains(state5));

            Assert.AreEqual(1, automaton.FinalStates.Count);
            Assert.AreEqual(state5, automaton.FinalStates[0]);

            Assert.AreEqual(0, automaton.EdgeValue(state4, state4).Count);
            //Assert.AreEqual(0, automaton.EdgeValue(state4, state4)[0].Count);

            Assert.AreEqual(1, automaton.EdgeValue(state4, state5).Count);
            //Assert.AreEqual(0, automaton.EdgeValue(state4, state5)[0].Count);

            Assert.AreEqual(1, automaton.EdgeValue(state5, state4).Count);
            Assert.IsTrue(automaton.EdgeValue(state5, state4).Contains(p));

            Assert.AreEqual(1, automaton.EdgeValue(state5, state5).Count);
            Assert.IsTrue(automaton.EdgeValue(state5, state5).Contains(p));
        }

        [TestMethod]
        public void Buchi_automaton_by_LTL_formula_3()
        {
            var p = new AtomicPredicate("p");
            var x = new X(p);

            var automaton = BuchiAutomatonBuilder.Build(x);

            var state1 = automaton.States[0];
            var state2 = automaton.States[1];
            var state3 = automaton.States[2];
            var state4 = automaton.States[3];

            Assert.AreEqual(4, automaton.States.Count);

            Assert.AreEqual(0, automaton.StateValue(state1).Count());
            Assert.AreEqual(1, automaton.StateValue(state2).Count());
            Assert.IsTrue(automaton.StateValue(state2).Contains(p));
            Assert.AreEqual(1, automaton.StateValue(state3).Count());
            Assert.IsTrue(automaton.StateValue(state3).Contains(x));
            Assert.AreEqual(2, automaton.StateValue(state4).Count());
            Assert.IsTrue(automaton.StateValue(state4).Contains(x));
            Assert.IsTrue(automaton.StateValue(state4).Contains(p));

            Assert.AreEqual(2, automaton.InitStates.Count);
            Assert.IsTrue(automaton.InitStates.Contains(state3));
            Assert.IsTrue(automaton.InitStates.Contains(state4));

            Assert.AreEqual(0, automaton.FinalStates.Count);
        }

        /*[TestMethod]
        public void Buchi_automaton_by_LTL_formula_4()
        {
            var p = new AtomicPredicate("p");
            var g = new G(p);
            var f = new F(g);

            var automaton = BuchiAutomatonBuilder.Build(f);

            var state1 = automaton.States[0];
            var state2 = automaton.States[1];
            var state3 = automaton.States[2];
            var state4 = automaton.States[3];
            var state5 = automaton.States[4];

            Assert.AreEqual(5, automaton.States.Count);

            Assert.AreEqual(1, automaton.StateValue(state3).Count());
            Assert.AreEqual(2, automaton.StateValue(state4).Count());
            Assert.AreEqual(3, automaton.StateValue(state5).Count());
            Assert.IsTrue(automaton.StateValue(state3).Contains(f));
            Assert.IsTrue(automaton.StateValue(state4).Contains(p));
            Assert.IsTrue(automaton.StateValue(state4).Contains());
            Assert.IsTrue(automaton.StateValue(state5).Contains(p));
            Assert.IsTrue(automaton.StateValue(state5).Contains(f));
            Assert.IsTrue(automaton.StateValue(state5).Contains(g));

            Assert.AreEqual(2, automaton.InitStates.Count);
            Assert.IsTrue(automaton.InitStates.Contains(state4));
            Assert.IsTrue(automaton.InitStates.Contains(state5));

            Assert.AreEqual(1, automaton.FinalStates.Count);
            Assert.AreEqual(state5, automaton.FinalStates[0]);

            Assert.AreEqual(1, automaton.EdgeValue(state4, state4).Count);
            Assert.AreEqual(0, automaton.EdgeValue(state4, state4)[0].Count);

            Assert.AreEqual(1, automaton.EdgeValue(state4, state5).Count);
            Assert.AreEqual(0, automaton.EdgeValue(state4, state5)[0].Count);

            Assert.AreEqual(1, automaton.EdgeValue(state5, state4).Count);
            Assert.AreEqual(1, automaton.EdgeValue(state5, state4)[0].Count);
            Assert.IsTrue(automaton.EdgeValue(state5, state4)[0].Contains(p));

            Assert.AreEqual(1, automaton.EdgeValue(state5, state5).Count);
            Assert.AreEqual(1, automaton.EdgeValue(state5, state5)[0].Count);
            Assert.IsTrue(automaton.EdgeValue(state5, state5)[0].Contains(p));
        }*/

    }
}
