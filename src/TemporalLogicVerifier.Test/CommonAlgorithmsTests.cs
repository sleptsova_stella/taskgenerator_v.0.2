﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemporalLogicVerifier.Common;
using TemporalLogicVerifier.LTLFormula;

namespace TemporalLogicVerifier.Test
{
    [TestClass]
    public class CommonAlgorithmsTests
    {
        [TestMethod]
        public void Get_all_ltl_subformulas_test_1()
        {
            var p = new AtomicPredicate("p");
            var q = new AtomicPredicate("q");
            var p_and_q = new And(p, q);
            var p_or_q =  new Or(p, q);
            var formula = new U(p_and_q, p_or_q);

            var result = AlgorithmsHelper.GetAllSubFormulas(formula);

            Assert.AreEqual(5, result.Count);
            Assert.IsTrue(result.Contains(p));
            Assert.IsTrue(result.Contains(q));
            Assert.IsTrue(result.Contains(p_and_q));
            Assert.IsTrue(result.Contains(p_or_q));
            Assert.IsTrue(result.Contains(formula));
        }

        [TestMethod]
        public void Get_all_ltl_subformulas_test_2()
        {
            var a = new AtomicPredicate("a");
            var b = new AtomicPredicate("b");
            var a_or_b = new Or(a, b);
            var G_a = new G(a);
            var formula = new U(a_or_b, G_a);

            var result = AlgorithmsHelper.GetAllSubFormulas(formula);

            Assert.AreEqual(5, result.Count);
            Assert.IsTrue(result.Contains(a));
            Assert.IsTrue(result.Contains(b));
            Assert.IsTrue(result.Contains(a_or_b));
            Assert.IsTrue(result.Contains(G_a));
            Assert.IsTrue(result.Contains(formula));
        }
        
    }
}
