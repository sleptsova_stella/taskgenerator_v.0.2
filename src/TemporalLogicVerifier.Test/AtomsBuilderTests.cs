﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemporalLogicVerifier.LTLBuilders;
using TemporalLogicVerifier.LTLFormula;

namespace TemporalLogicVerifier.Test
{
    [TestClass]
    public class AtomsBuilderTests
    {
        [TestMethod]
        public void Test()
        {
            var palletIds = new string[20];

            for (var i= 0; i < 20; i++)
            {
                palletIds[i] = "";
            }

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Atoms_builder_1()
        {
            var p = new AtomicPredicate("p");
            var f = new F(p);

            var atoms = AtomsBuilder.Build(f);

            Assert.AreEqual(3, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(1, atoms[1].Count);
            Assert.AreEqual(2, atoms[2].Count);
            Assert.IsTrue(atoms[1].Contains(f));
            Assert.IsTrue(atoms[2].Contains(p));
            Assert.IsTrue(atoms[2].Contains(f));
        }

        [TestMethod]
        public void Atoms_builder_2()
        {
            var p = new AtomicPredicate("p");
            var f = new F(p);
            var g = new G(f);

            var atoms = AtomsBuilder.Build(g);

            Assert.AreEqual(5, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(1, atoms[1].Count);
            Assert.AreEqual(2, atoms[2].Count);
            Assert.AreEqual(2, atoms[3].Count);
            Assert.AreEqual(3, atoms[4].Count);

            Assert.IsTrue(atoms[1].Contains(f));
            Assert.IsTrue(atoms[2].Contains(p));
            Assert.IsTrue(atoms[2].Contains(f));
            Assert.IsTrue(atoms[3].Contains(f));
            Assert.IsTrue(atoms[3].Contains(g));
            Assert.IsTrue(atoms[4].Contains(p));
            Assert.IsTrue(atoms[4].Contains(f));
            Assert.IsTrue(atoms[4].Contains(g));
        }

        [TestMethod]
        public void Atoms_builder_3()
        {
            var p = new AtomicPredicate("p");
            var q = new AtomicPredicate("q");
            var p_or_q = new Or(p,q);
            var p_and_q = new And(p,q);
            var u = new U(p_or_q, p_and_q);

            var atoms = AtomsBuilder.Build(u);

            Assert.AreEqual(6, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(2, atoms[1].Count);
            Assert.AreEqual(2, atoms[2].Count);
            Assert.AreEqual(3, atoms[3].Count);
            Assert.AreEqual(3, atoms[4].Count);
            Assert.AreEqual(5, atoms[5].Count);

            Assert.IsTrue(atoms[1].Contains(p));
            Assert.IsTrue(atoms[1].Contains(p_or_q));

            Assert.IsTrue(atoms[2].Contains(q));
            Assert.IsTrue(atoms[2].Contains(p_or_q));

            Assert.IsTrue(atoms[3].Contains(p));
            Assert.IsTrue(atoms[3].Contains(p_or_q));
            Assert.IsTrue(atoms[3].Contains(u));

            Assert.IsTrue(atoms[4].Contains(q));
            Assert.IsTrue(atoms[4].Contains(p_or_q));
            Assert.IsTrue(atoms[4].Contains(u));

            Assert.IsTrue(atoms[5].Contains(p));
            Assert.IsTrue(atoms[5].Contains(q));
            Assert.IsTrue(atoms[5].Contains(p_and_q));
            Assert.IsTrue(atoms[5].Contains(p_or_q));
            Assert.IsTrue(atoms[5].Contains(u));
        }

        [TestMethod]
        public void Atoms_builder_4()
        {
            var p = new AtomicPredicate("p");
            var q = new AtomicPredicate("q");
            var u = new U(p, q);

            var atoms = AtomsBuilder.Build(u);

            Assert.AreEqual(5, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(1, atoms[1].Count);
            Assert.AreEqual(2, atoms[2].Count);
            Assert.AreEqual(2, atoms[3].Count);
            Assert.AreEqual(3, atoms[4].Count);

            Assert.IsTrue(atoms[1].Contains(p));

            Assert.IsTrue(atoms[2].Contains(p));
            Assert.IsTrue(atoms[2].Contains(u));

            Assert.IsTrue(atoms[3].Contains(q));
            Assert.IsTrue(atoms[3].Contains(u));

            Assert.IsTrue(atoms[4].Contains(q));
            Assert.IsTrue(atoms[4].Contains(p));
            Assert.IsTrue(atoms[4].Contains(u));
        }

        [TestMethod]
        public void Atoms_builder_5()
        {
            var p = new AtomicPredicate("p");
            var x = new X(p);

            var atoms = AtomsBuilder.Build(x);

            Assert.AreEqual(4, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(1, atoms[1].Count);
            Assert.AreEqual(1, atoms[2].Count);
            Assert.AreEqual(2, atoms[3].Count);

            Assert.IsTrue(atoms[1].Contains(p));

            Assert.IsTrue(atoms[2].Contains(x));

            Assert.IsTrue(atoms[3].Contains(x));
            Assert.IsTrue(atoms[3].Contains(p));
        }

        [TestMethod]
        public void Atoms_builder_6()
        {
            var a = new AtomicPredicate("a");
            var g = new G(a);
            var f = new F(g);

            var atoms = AtomsBuilder.Build(f);

            Assert.AreEqual(5, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(1, atoms[1].Count);
            Assert.AreEqual(1, atoms[2].Count);
            Assert.AreEqual(2, atoms[3].Count);
            Assert.AreEqual(3, atoms[4].Count);

            Assert.IsTrue(atoms[1].Contains(a));

            Assert.IsTrue(atoms[2].Contains(f));

            Assert.IsTrue(atoms[3].Contains(a));
            Assert.IsTrue(atoms[3].Contains(f));

            Assert.IsTrue(atoms[4].Contains(a));
            Assert.IsTrue(atoms[4].Contains(g));
            Assert.IsTrue(atoms[4].Contains(f));
        }

        [TestMethod]
        public void Atoms_builder_7()
        {
            var a = new AtomicPredicate("a");
            var b = new AtomicPredicate("b");
            var a_or_b = new Or(a, b);
            var g = new G(a);
            var u = new U(a_or_b,g);

            var atoms = AtomsBuilder.Build(u);

            Assert.AreEqual(9, atoms.Count);
            Assert.AreEqual(0, atoms[0].Count);
            Assert.AreEqual(2, atoms[1].Count);
            Assert.AreEqual(2, atoms[2].Count);
            Assert.AreEqual(3, atoms[3].Count);
            Assert.AreEqual(3, atoms[4].Count);
            Assert.AreEqual(3, atoms[5].Count);
            Assert.AreEqual(4, atoms[6].Count);
            Assert.AreEqual(4, atoms[7].Count);
            Assert.AreEqual(5, atoms[8].Count);

            Assert.IsTrue(atoms[1].Contains(a));
            Assert.IsTrue(atoms[1].Contains(a_or_b));

            Assert.IsTrue(atoms[2].Contains(b));
            Assert.IsTrue(atoms[2].Contains(a_or_b));

            Assert.IsTrue(atoms[3].Contains(a));
            Assert.IsTrue(atoms[3].Contains(b));
            Assert.IsTrue(atoms[3].Contains(a_or_b));

            Assert.IsTrue(atoms[4].Contains(a));
            Assert.IsTrue(atoms[4].Contains(a_or_b));
            Assert.IsTrue(atoms[4].Contains(u));

            Assert.IsTrue(atoms[5].Contains(b));
            Assert.IsTrue(atoms[5].Contains(a_or_b));
            Assert.IsTrue(atoms[5].Contains(u));

            Assert.IsTrue(atoms[6].Contains(a));
            Assert.IsTrue(atoms[6].Contains(b));
            Assert.IsTrue(atoms[6].Contains(a_or_b));
            Assert.IsTrue(atoms[6].Contains(u));

            Assert.IsTrue(atoms[7].Contains(a));
            Assert.IsTrue(atoms[7].Contains(a_or_b));
            Assert.IsTrue(atoms[7].Contains(g));
            Assert.IsTrue(atoms[7].Contains(u));

            Assert.IsTrue(atoms[8].Contains(a));
            Assert.IsTrue(atoms[8].Contains(b));
            Assert.IsTrue(atoms[8].Contains(a_or_b));
            Assert.IsTrue(atoms[8].Contains(g));
            Assert.IsTrue(atoms[8].Contains(u));
        }
    }
}
