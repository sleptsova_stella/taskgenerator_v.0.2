﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;
using CTL = TemporalLogicVerifier.CTLFormula;
using LTL = TemporalLogicVerifier.LTLFormula;
using TemporalLogicVerifier.KripkeStructure;
using TemporalLogicVerifier.LTLBuilders;

namespace TemporalLogicVerifier.Test
{
    [TestClass]
    public class BuchiAutomatonBuilderTest
    {
        private readonly IState _state_s0 = new State("s0");
        private readonly IState _state_s1 = new State("s1");
        private readonly IState _state_s2 = new State("s2");
        private readonly IState _state_s3 = new State("s3");

        private readonly ICTLFormula _atomicPredicate_q = new CTL.AtomicPredicate("q");
        private readonly ICTLFormula _atomicPredicate_r = new CTL.AtomicPredicate("r");

        private readonly ILTLFormula _ltl_atomicPredicate_q = new LTL.AtomicPredicate("q");
        private readonly ILTLFormula _ltl_atomicPredicate_r = new LTL.AtomicPredicate("r");

        private TotalRatio _total_ratio_01;
        private TotalRatio _total_ratio_03;
        private TotalRatio _total_ratio_12;
        private TotalRatio _total_ratio_23;
        private TotalRatio _total_ratio_31;
        private TotalRatio _total_ratio_22;

        private IKripkeStructure _kripkeStructure;
        private ILtlKripkeStructure _ltlKripkeStructure;

        [TestInitialize]
        public void TestInitialize()
        {
            var states = new IState[] { _state_s0, _state_s1, _state_s2, _state_s3 };
            var initialStates = new IState[] { _state_s0 };

            _total_ratio_01 = new TotalRatio(_state_s0, _state_s1);
            _total_ratio_03 = new TotalRatio(_state_s0, _state_s3);
            _total_ratio_12 = new TotalRatio(_state_s1, _state_s2);
            _total_ratio_23 = new TotalRatio(_state_s2, _state_s3);
            _total_ratio_31 = new TotalRatio(_state_s3, _state_s1);
            _total_ratio_22 = new TotalRatio(_state_s2, _state_s2);

            var totalRatio = new ITotalRatio[]
                {
                    _total_ratio_01,
                    _total_ratio_03,
                    _total_ratio_12,
                    _total_ratio_23,
                    _total_ratio_31,
                    _total_ratio_22
                };

            var markFunction = new Dictionary<IState, IList<ICTLFormula>>()
            {
                { _state_s0, new ICTLFormula[]{ _atomicPredicate_q } },
                { _state_s1, new ICTLFormula[]{ _atomicPredicate_q, _atomicPredicate_r } },
                { _state_s2, new ICTLFormula[]{ _atomicPredicate_r } },
                { _state_s3, new ICTLFormula[]{ } },
            };

            var ltlMarkFunction = new Dictionary<IState, IList<ILTLFormula>>()
            {
                { _state_s0, new ILTLFormula[]{ _ltl_atomicPredicate_q } },
                { _state_s1, new ILTLFormula[]{ _ltl_atomicPredicate_q, _ltl_atomicPredicate_r } },
                { _state_s2, new ILTLFormula[]{ _ltl_atomicPredicate_r } },
                { _state_s3, new ILTLFormula[]{ } },
            };

            _kripkeStructure = new KripkeStructure.KripkeStructure(
                states,
                initialStates,
                totalRatio,
                new ICTLFormula[] { _atomicPredicate_q, _atomicPredicate_r },
                markFunction);

            _ltlKripkeStructure = new LtlKripkeStructure(
                states,
                initialStates,
                totalRatio,
                new ILTLFormula[] {_ltl_atomicPredicate_q, _ltl_atomicPredicate_r},
                ltlMarkFunction);
        }

        [TestMethod]
        public void Build_buchi_automaton_from_kripke_structure()
        {
            var buchiAutomaton = BuchiAutomatonBuilder.Build(_ltlKripkeStructure);

            var alphabet = buchiAutomaton.Alphabet;

            Assert.AreEqual(4, alphabet.Count); // with empty
            Assert.AreEqual("qrqr", string.Join("", alphabet.Select(x => string.Join("", x.Select(l => ((LTLFormula.AtomicPredicate)l).Name)))));

            Assert.AreEqual("q", ((LTLFormula.AtomicPredicate)(buchiAutomaton.EdgeValue(_total_ratio_01)).FirstOrDefault()).Name);
            Assert.AreEqual("q", ((LTLFormula.AtomicPredicate)(buchiAutomaton.EdgeValue(_total_ratio_03)).FirstOrDefault()).Name);
            Assert.AreEqual("qr", string.Join("", buchiAutomaton.EdgeValue(_total_ratio_12).Select(x => ((LTLFormula.AtomicPredicate)x).Name)));
            Assert.AreEqual(null, buchiAutomaton.EdgeValue(_total_ratio_31).FirstOrDefault());
            Assert.AreEqual("r", ((LTLFormula.AtomicPredicate)(buchiAutomaton.EdgeValue(_total_ratio_23)).FirstOrDefault()).Name);
            Assert.AreEqual("r", ((LTLFormula.AtomicPredicate)(buchiAutomaton.EdgeValue(_total_ratio_22)).FirstOrDefault()).Name);
        }
    }
}