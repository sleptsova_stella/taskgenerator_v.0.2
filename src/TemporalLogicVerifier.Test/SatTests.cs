﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.CTLFormula;
using TemporalLogicVerifier.KripkeStructure;

namespace TemporalLogicVerifier.Test
{
    [TestClass]
    public class SatTests
    {
        private readonly IState _state_s0 = new State("s0");
        private readonly IState _state_s1 = new State("s1");
        private readonly IState _state_s2 = new State("s2");
        private readonly IState _state_s3 = new State("s3");
        private readonly IState _state_s4 = new State("s4");
        private readonly IState _state_s5 = new State("s5");

        private readonly ICTLFormula _atomicPredicate_p = new AtomicPredicate("p");
        private readonly ICTLFormula _atomicPredicate_q = new AtomicPredicate("q");
        private readonly ICTLFormula _atomicPredicate_r = new AtomicPredicate("r");

        private IKripkeStructure _kripkeStructure;

        [TestInitialize]
        public void SatTestsInit()
        {
            var states = new IState[] { _state_s0, _state_s1, _state_s2, _state_s3, _state_s4, _state_s5 };
            var initialStates = new IState[] { _state_s0 };

            var totalRatio = new ITotalRatio[]
                {
                    new TotalRatio(_state_s0,_state_s1),
                    new TotalRatio(_state_s0,_state_s3),
                    new TotalRatio(_state_s1,_state_s4),
                    new TotalRatio(_state_s4,_state_s0),
                    new TotalRatio(_state_s4,_state_s3),
                    new TotalRatio(_state_s4,_state_s5),
                    new TotalRatio(_state_s3,_state_s0),
                    new TotalRatio(_state_s3,_state_s5),
                    new TotalRatio(_state_s5,_state_s2),
                    new TotalRatio(_state_s2,_state_s1),
                    new TotalRatio(_state_s2,_state_s3)
                };
            var markFunction = new Dictionary<IState, IList<ICTLFormula>>()
            {
                { _state_s0, new List<ICTLFormula>(){ } },
                { _state_s1, new List<ICTLFormula>(){ _atomicPredicate_p } },
                { _state_s2, new List<ICTLFormula>(){ _atomicPredicate_p } },
                { _state_s3, new List<ICTLFormula>(){ _atomicPredicate_p } },
                { _state_s4, new List<ICTLFormula>(){ _atomicPredicate_p, _atomicPredicate_q } },
                { _state_s5, new List<ICTLFormula>(){ _atomicPredicate_p, _atomicPredicate_r } },
            };

            _kripkeStructure = new KripkeStructure.KripkeStructure(
                states,
                initialStates,
                totalRatio,
                new ICTLFormula[] { _atomicPredicate_p, _atomicPredicate_q, _atomicPredicate_r },
                markFunction);

        }

        [TestMethod]
        public void SAT_EX()
        {
            var ex_p = new EX(_atomicPredicate_p);
            var ex_q = new EX(_atomicPredicate_q);
            var ex_r = new EX(_atomicPredicate_r);

            var states_sat_ex_p = ex_p.Sat(_kripkeStructure);
            var states_sat_ex_q = ex_q.Sat(_kripkeStructure);
            var states_sat_ex_r = ex_r.Sat(_kripkeStructure);

            Assert.IsTrue(states_sat_ex_p.Contains(_state_s0));
            Assert.IsTrue(states_sat_ex_p.Contains(_state_s1));
            Assert.IsTrue(states_sat_ex_p.Contains(_state_s2));
            Assert.IsTrue(states_sat_ex_p.Contains(_state_s3));
            Assert.IsTrue(states_sat_ex_p.Contains(_state_s4));
            Assert.IsTrue(states_sat_ex_p.Contains(_state_s5));

            Assert.IsFalse(states_sat_ex_q.Contains(_state_s0));
            Assert.IsTrue(states_sat_ex_q.Contains(_state_s1));
            Assert.IsFalse(states_sat_ex_q.Contains(_state_s2));
            Assert.IsFalse(states_sat_ex_q.Contains(_state_s0));
            Assert.IsFalse(states_sat_ex_q.Contains(_state_s3));
            Assert.IsFalse(states_sat_ex_q.Contains(_state_s4));
            Assert.IsFalse(states_sat_ex_q.Contains(_state_s5));

            Assert.IsFalse(states_sat_ex_r.Contains(_state_s0));
            Assert.IsFalse(states_sat_ex_r.Contains(_state_s1));
            Assert.IsFalse(states_sat_ex_r.Contains(_state_s2));
            Assert.IsTrue(states_sat_ex_r.Contains(_state_s3));
            Assert.IsTrue(states_sat_ex_r.Contains(_state_s4));
            Assert.IsFalse(states_sat_ex_r.Contains(_state_s5));
        }

        [TestMethod]
        public void SAT_Predicate()
        {
            var states_sat_p = _atomicPredicate_p.Sat(_kripkeStructure);

            Assert.IsFalse(states_sat_p.Contains(_state_s0));
            Assert.IsTrue(states_sat_p.Contains(_state_s1));
            Assert.IsTrue(states_sat_p.Contains(_state_s2));
            Assert.IsTrue(states_sat_p.Contains(_state_s3));
            Assert.IsTrue(states_sat_p.Contains(_state_s4));
            Assert.IsTrue(states_sat_p.Contains(_state_s5));
        }

        [TestMethod]
        public void SAT_NegativePredicate()
        {
            var negative_p = new NegativeFormula(_atomicPredicate_p);
            var states_sat_non_p = negative_p.Sat(_kripkeStructure);

            Assert.IsTrue(states_sat_non_p.Contains(_state_s0));
            Assert.IsFalse(states_sat_non_p.Contains(_state_s1));
            Assert.IsFalse(states_sat_non_p.Contains(_state_s2));
            Assert.IsFalse(states_sat_non_p.Contains(_state_s3));
            Assert.IsFalse(states_sat_non_p.Contains(_state_s4));
            Assert.IsFalse(states_sat_non_p.Contains(_state_s5));
        }

        [TestMethod]
        public void SAT_OR()
        {
            var p_or_r = new Or(_atomicPredicate_p, _atomicPredicate_q);
            var states_sat_p_or_r = p_or_r.Sat(_kripkeStructure);

            Assert.IsFalse(states_sat_p_or_r.Contains(_state_s0));
            Assert.IsTrue(states_sat_p_or_r.Contains(_state_s1));
            Assert.IsTrue(states_sat_p_or_r.Contains(_state_s2));
            Assert.IsTrue(states_sat_p_or_r.Contains(_state_s3));
            Assert.IsTrue(states_sat_p_or_r.Contains(_state_s4));
            Assert.IsTrue(states_sat_p_or_r.Contains(_state_s5));
        }

        [TestMethod]
        public void SAT_AF()
        {
            var af_p = new AF(_atomicPredicate_p);
            var af_q = new AF(_atomicPredicate_q);
            var af_r = new AF(_atomicPredicate_r);

            var states_sat_af_p = af_p.Sat(_kripkeStructure);
            var states_sat_af_q = af_q.Sat(_kripkeStructure);
            var states_sat_af_r = af_r.Sat(_kripkeStructure);

            Assert.IsTrue(states_sat_af_p.Contains(_state_s0));
            Assert.IsTrue(states_sat_af_p.Contains(_state_s1));
            Assert.IsTrue(states_sat_af_p.Contains(_state_s2));
            Assert.IsTrue(states_sat_af_p.Contains(_state_s3));
            Assert.IsTrue(states_sat_af_p.Contains(_state_s4));
            Assert.IsTrue(states_sat_af_p.Contains(_state_s5));

            Assert.IsFalse(states_sat_af_q.Contains(_state_s0));
            Assert.IsTrue(states_sat_af_q.Contains(_state_s1));
            Assert.IsFalse(states_sat_af_q.Contains(_state_s2));
            Assert.IsFalse(states_sat_af_q.Contains(_state_s3));
            Assert.IsTrue(states_sat_af_q.Contains(_state_s4));
            Assert.IsFalse(states_sat_af_q.Contains(_state_s5));

            Assert.IsFalse(states_sat_af_r.Contains(_state_s0));
            Assert.IsFalse(states_sat_af_r.Contains(_state_s1));
            Assert.IsFalse(states_sat_af_r.Contains(_state_s2));
            Assert.IsFalse(states_sat_af_r.Contains(_state_s3));
            Assert.IsFalse(states_sat_af_r.Contains(_state_s4));
            Assert.IsTrue(states_sat_af_r.Contains(_state_s5));
        }

        [TestMethod]
        public void SAT_EU()
        {
            var eu_pq = new EU(_atomicPredicate_p, _atomicPredicate_q);
            var eu_qp = new EU(_atomicPredicate_q, _atomicPredicate_p);
            var eu_pr = new EU(_atomicPredicate_p, _atomicPredicate_r);
            var eu_rp = new EU(_atomicPredicate_r, _atomicPredicate_p);
            var eu_qr = new EU(_atomicPredicate_q, _atomicPredicate_r);
            var eu_rq = new EU(_atomicPredicate_r, _atomicPredicate_q);

            var states_sat_eu_pq = eu_pq.Sat(_kripkeStructure);
            var states_sat_eu_qp = eu_qp.Sat(_kripkeStructure);
            var states_sat_eu_pr = eu_pr.Sat(_kripkeStructure);
            var states_sat_eu_rp = eu_rp.Sat(_kripkeStructure);
            var states_sat_eu_qr = eu_qr.Sat(_kripkeStructure);
            var states_sat_eu_rq = eu_rq.Sat(_kripkeStructure);

            Assert.IsFalse(states_sat_eu_pq.Contains(_state_s0));
            Assert.IsTrue(states_sat_eu_pq.Contains(_state_s1));
            Assert.IsTrue(states_sat_eu_pq.Contains(_state_s2));
            Assert.IsTrue(states_sat_eu_pq.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_pq.Contains(_state_s4));
            Assert.IsTrue(states_sat_eu_pq.Contains(_state_s5));

            Assert.IsFalse(states_sat_eu_qp.Contains(_state_s0));
            Assert.IsTrue(states_sat_eu_qp.Contains(_state_s1));
            Assert.IsTrue(states_sat_eu_qp.Contains(_state_s2));
            Assert.IsTrue(states_sat_eu_qp.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_qp.Contains(_state_s4));
            Assert.IsTrue(states_sat_eu_qp.Contains(_state_s5));

            Assert.IsFalse(states_sat_eu_pr.Contains(_state_s0));
            Assert.IsTrue(states_sat_eu_pr.Contains(_state_s1));
            Assert.IsTrue(states_sat_eu_pr.Contains(_state_s2));
            Assert.IsTrue(states_sat_eu_pr.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_pr.Contains(_state_s4));
            Assert.IsTrue(states_sat_eu_pr.Contains(_state_s5));

            Assert.IsFalse(states_sat_eu_rp.Contains(_state_s0));
            Assert.IsTrue(states_sat_eu_rp.Contains(_state_s1));
            Assert.IsTrue(states_sat_eu_rp.Contains(_state_s2));
            Assert.IsTrue(states_sat_eu_rp.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_rp.Contains(_state_s4));
            Assert.IsTrue(states_sat_eu_rp.Contains(_state_s5));

            Assert.IsFalse(states_sat_eu_qr.Contains(_state_s0));
            Assert.IsFalse(states_sat_eu_qr.Contains(_state_s1));
            Assert.IsFalse(states_sat_eu_qr.Contains(_state_s2));
            Assert.IsFalse(states_sat_eu_qr.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_qr.Contains(_state_s4));
            Assert.IsTrue(states_sat_eu_qr.Contains(_state_s5));

            Assert.IsFalse(states_sat_eu_rq.Contains(_state_s0));
            Assert.IsFalse(states_sat_eu_rq.Contains(_state_s1));
            Assert.IsFalse(states_sat_eu_rq.Contains(_state_s2));
            Assert.IsFalse(states_sat_eu_rq.Contains(_state_s3));
            Assert.IsTrue(states_sat_eu_rq.Contains(_state_s4));
            Assert.IsFalse(states_sat_eu_rq.Contains(_state_s5));
        }

        [TestMethod]
        public void CTL_model_checker()
        {
            var non_p = new NegativeFormula(_atomicPredicate_p);
            var q_or_r = new Or(_atomicPredicate_q, _atomicPredicate_r);
            var ex_non_p = new EX(non_p);
            var af_q_or_r = new AF(q_or_r);
            var formula = new EU(ex_non_p, af_q_or_r);

            var result = _kripkeStructure.ModelChecker(formula);
            Assert.AreEqual(false, result);
        }
    }
}
