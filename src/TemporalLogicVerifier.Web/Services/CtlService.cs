﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Web.Contract.Mapping;
using TemporalLogicVerifier.Web.Contract.Models;
using TemporalLogicVerifier.Web.Contract.Services;

namespace TemporalLogicVerifier.Web.Services
{
    public class CtlService : ICtlService
    {
        //private static IDictionary<string, UserInfo> _userInfoByUserId = new Dictionary<string, UserInfo>();
        //private static IDictionary<string, MainCtlModel> _ctlModelInfoByUserId = new Dictionary<string, MainCtlModel>();

        private readonly TimeSpan _userInfoTimeSpan = TimeSpan.FromMinutes(20);

        private readonly IKripkeStructureGenerator _kripkeStructureGenerator;
        private readonly ICtlFormulaGenerator _ctlFormulaGenerator;
        private readonly IMemoryCache _memoryCache;

        public CtlService(
            IKripkeStructureGenerator kripkeStructureGenerator,
            ICtlFormulaGenerator ctlFormulaGenerator,
            IMemoryCache memoryCache)
        {
            _kripkeStructureGenerator = kripkeStructureGenerator ?? throw new ArgumentNullException(nameof(kripkeStructureGenerator));
            _ctlFormulaGenerator = ctlFormulaGenerator ?? throw new ArgumentNullException(nameof(ctlFormulaGenerator));
            _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));
        }

        public Task<MainCtlModel> GenerateMainCtlModel(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }


            GetUserInfo(userId);
            var newModel = GetMainModel(userId);
            return Task.FromResult(newModel);
        }

        private UserInfo GetUserInfo(string userId)
        {
            return
                _memoryCache.GetOrCreate(userId + "info", cacheEntry =>
                {
                    cacheEntry.SetAbsoluteExpiration(_userInfoTimeSpan);
                    return new UserInfo(userId, false);
                });
        }

        private MainCtlModel GetMainModel(string userId)
        {
            return
                 _memoryCache.GetOrCreate(userId + "model", cacheEntry =>
                {
                    cacheEntry.SetAbsoluteExpiration(_userInfoTimeSpan);
                    return GenerateNewMainCtlModel();
                });
        }

        public Task<bool> Verify(string userId, int[] formulaNumbers)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }

            var mainModel = GetMainModel(userId);
            var userInfo = GetUserInfo(userId);
            if (!userInfo.ModelCheckingWasRunning)
            {
                var formula = mainModel.CtlFormula;
                mainModel.KripkeStructureValue.ModelChecker(mainModel.CtlFormulaValue);
                userInfo.ModelCheckingWasRunning = true;
            }

            var checkingFormulas = new List<ICTLFormula>();
            foreach (var i in formulaNumbers)
            {
                checkingFormulas.Add(mainModel.CtlFormula.NumericSuCtlFormulas[i]);
            }

            var result =  CheckMarkedsStates(
                mainModel.KripkeStructureValue,
                mainModel.CheckingState,
                checkingFormulas);

            return Task.FromResult(result);
        }

        private bool CheckMarkedsStates(
            IKripkeStructure kripkeStructure,
            IState state,
            IReadOnlyList<ICTLFormula> formulas)
        {
            var ksState = kripkeStructure.States.FirstOrDefault(x => x.Name == state.Name);
            if (ksState == null)
            {
                throw new Exception($"\"{state.Name}\" state isn't contained in the current kripke structure");
            }

            var markedFormulas = kripkeStructure.MarkFunction(ksState);

            if ((markedFormulas == null || !markedFormulas.Any()) && (formulas == null || !formulas.Any()))
            {
                return true;
            }

            if (markedFormulas.Count != formulas.Count || markedFormulas.Any(x => formulas.FirstOrDefault(y => y.Equals(x)) != null))
            {
                return false;
            }

            var formulasContains = formulas.ToDictionary(x => x, x => false);
            foreach (var formula in formulas)
            {
                var f = markedFormulas.FirstOrDefault(x => x.Equals(formula));
                if (f == null)
                {
                    return false;
                }
                formulasContains[f] = true;
            }

            if (formulasContains.Values.Contains(false))
            {
                return false;
            }

            return true;
        }

        private MainCtlModel GenerateNewMainCtlModel()
        {
            var ctlFormula = _ctlFormulaGenerator.InitRandom();
            var atomicPredicatesUsedByFormula = _ctlFormulaGenerator.GetAtomicPredicatesUsedByFormula(ctlFormula);
            var kripkeStructure = _kripkeStructureGenerator.InitRandom(atomicPredicatesUsedByFormula);
            var checkingState = _kripkeStructureGenerator.GetCheckingState(kripkeStructure);

            return new MainCtlModel(ctlFormula, kripkeStructure, checkingState, ctlFormula.ToBl(), kripkeStructure.ToBl());
        }
    }
}