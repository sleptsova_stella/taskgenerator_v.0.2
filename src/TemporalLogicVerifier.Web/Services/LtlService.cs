﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.LTLBuilders;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Web.Contract.Mapping;
using TemporalLogicVerifier.Web.Contract.Models;
using TemporalLogicVerifier.Web.Contract.Services;

namespace TemporalLogicVerifier.Web.Services
{
    public class LtlService : ILtlService
    {
        private readonly TimeSpan _userInfoTimeSpan = TimeSpan.FromMinutes(20);

        private readonly IKripkeStructureGenerator _kripkeStructureGenerator;
        private readonly ILtlFormulaGenerator _ltlFormulaGenerator;
        private readonly IMemoryCache _memoryCache;

        public LtlService(
            IKripkeStructureGenerator kripkeStructureGenerator,
            ILtlFormulaGenerator ltlFormulaGenerator,
            IMemoryCache memoryCache)
        {
            _kripkeStructureGenerator = kripkeStructureGenerator ?? throw new ArgumentNullException(nameof(kripkeStructureGenerator));
            _ltlFormulaGenerator = ltlFormulaGenerator ?? throw new ArgumentNullException(nameof(ltlFormulaGenerator));
            _memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(memoryCache));
        }

        public Task<MainLtlModel> GenerateMainLtlModel(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }


            GetUserInfo(userId);
            var newModel = GetMainModel(userId);
            return Task.FromResult(newModel);
        }

        public Task<bool> Verify(string userId, string checkedStates)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }

            var mainModel = GetMainModel(userId);
            var result = IsCycle(mainModel, checkedStates);
            return Task.FromResult(result);
        }

        private bool IsCycle(MainLtlModel mainLtlModel, string checkingStates)
        {
            checkingStates = checkingStates.Replace(" ", string.Empty);
            var stateNames = checkingStates.Split('-');
            var directlyStates = stateNames
                .Select(x => mainLtlModel.SynchronousComposition.StateNumByStateName[x])
                .ToArray();

            var ks = mainLtlModel.KsValue;
            var ltl = mainLtlModel.LtlFormulaValue;
            var initState = mainLtlModel.SynchronousComposition.StateNumByStateName[$"{ks.InitStates.First().Name},{ltl.InitStates.First().Name}"];

            if (initState != directlyStates.First())
            {
                return false;
            }

            var scEdges = mainLtlModel.SynchronousComposition.Edges;
            /*var nextStateByState = new Dictionary<int,List<int>>();

            foreach (var currentState in mainLtlModel.SynchronousComposition.StateNumByStateName.Values)
            {
                nextStateByState[currentState] = new List<int>();
                foreach (var nextState in mainLtlModel.SynchronousComposition.StateNumByStateName.Values)
                {
                    if (scEdges[(currentState, nextState)])
                    {
                        nextStateByState[currentState].Add(nextState);
                    }
                }
            }*/

            for (var i = 0; i < directlyStates.Length; i++)
            {
                var currentState = directlyStates[i];
                var nextState = directlyStates[i+1];

                if (!scEdges[(currentState, nextState)])
                {
                    return false;
                }
            }

            return true;
        }

        private UserInfo GetUserInfo(string userId)
        {
            return
                _memoryCache.GetOrCreate(userId + "info", cacheEntry =>
                {
                    cacheEntry.SetAbsoluteExpiration(_userInfoTimeSpan);
                    return new UserInfo(userId, false);
                });
        }

        private MainLtlModel GetMainModel(string userId)
        {
            return
                 _memoryCache.GetOrCreate(userId + "model", cacheEntry =>
                {
                    cacheEntry.SetAbsoluteExpiration(_userInfoTimeSpan);
                    return GenerateNewMainLtlModel();
                });
        }

        /*public Task<bool> Verify(string userId, int[] formulaNumbers)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }

            var mainModel = GetMainModel(userId);
            var userInfo = GetUserInfo(userId);
            if (!userInfo.ModelCheckingWasRunning)
            {
                var formula = mainModel.CtlFormula;
                mainModel.KripkeStructureValue.ModelChecker(mainModel.CtlFormulaValue);
                userInfo.ModelCheckingWasRunning = true;
            }

            var checkingFormulas = new List<ICTLFormula>();
            foreach (var i in formulaNumbers)
            {
                checkingFormulas.Add(mainModel.CtlFormula.NumericSuCtlFormulas[i]);
            }

            var result =  CheckMarkedsStates(
                mainModel.KripkeStructureValue,
                mainModel.CheckingState,
                checkingFormulas);

            return Task.FromResult(result);
        }*/

        /*private bool CheckMarkedsStates(
            IKripkeStructure kripkeStructure,
            IState state,
            IReadOnlyList<ICTLFormula> formulas)
        {
            var ksState = kripkeStructure.States.FirstOrDefault(x => x.Name == state.Name);
            if (ksState == null)
            {
                throw new Exception($"\"{state.Name}\" state isn't contained in the current kripke structure");
            }

            var markedFormulas = kripkeStructure.MarkFunction(ksState);

            if ((markedFormulas == null || !markedFormulas.Any()) && (formulas == null || !formulas.Any()))
            {
                return true;
            }

            if (markedFormulas.Count != formulas.Count || markedFormulas.Any(x => formulas.FirstOrDefault(y => y.Equals(x)) != null))
            {
                return false;
            }

            var formulasContains = formulas.ToDictionary(x => x, x => false);
            foreach (var formula in formulas)
            {
                var f = markedFormulas.FirstOrDefault(x => x.Equals(formula));
                if (f == null)
                {
                    return false;
                }
                formulasContains[f] = true;
            }

            if (formulasContains.Values.Contains(false))
            {
                return false;
            }

            return true;
        }*/

        private MainLtlModel GenerateNewMainLtlModel()
        {
            var ltlFormula = _ltlFormulaGenerator.InitRandom();
            var atomicPredicatesUsedByFormula = _ltlFormulaGenerator.GetAtomicPredicatesUsedByFormula(ltlFormula);
            var ltlKripkeStructure = _kripkeStructureGenerator.InitRandom(atomicPredicatesUsedByFormula);

            var buchiAutomatonByLtl = BuchiAutomatonBuilder.Build(ltlFormula);
            var buchiAutomatonByKs = BuchiAutomatonBuilder.Build(ltlKripkeStructure);
            //var checkingState = _kripkeStructureGenerator.GetCheckingState(ltlKripkeStructure);
            var sc = GetSynchronousComposition(buchiAutomatonByLtl, buchiAutomatonByKs);

            return new MainLtlModel(
                buchiAutomatonByLtl,
                buchiAutomatonByKs,
                buchiAutomatonByLtl.ToBl(),
                buchiAutomatonByKs.ToBl(),
                sc);
        }

        private SynchronousComposition GetSynchronousComposition(IBuchiAutomaton ltl, IBuchiAutomaton ks)
        {
            var stateNumByStateName = new  Dictionary<string, int>();
            var ksStateAndLtlState = new List<(IState,IState)>();
            var scEdges  = new Dictionary<(int,int), bool>();

            var i = 1;
            foreach (var ltlState in ltl.States)
            {
                foreach (var ksState in ks.States)
                {
                    ksStateAndLtlState.Add((ksState, ltlState));
                    stateNumByStateName[$"{ksState.Name},{ltlState.Name}"] =  i;
                    i++;
                }
            }

            foreach (var s1s2_1 in ksStateAndLtlState)
            {
                var state1 = stateNumByStateName[$"{s1s2_1.Item1.Name},{s1s2_1.Item2.Name}"];
                foreach (var s1s2_2 in ksStateAndLtlState)
                {
                    var state2 = stateNumByStateName[$"{s1s2_2.Item1.Name},{s1s2_2.Item2.Name}"];
                    scEdges[(state1, state2)] = false;
                    if (ks.EdgeValue(s1s2_1.Item1, s1s2_2.Item1) != null &&
                        ltl.EdgeValue(s1s2_1.Item2, s1s2_2.Item2) != null)
                    {
                        scEdges[(state1, state2)] = true;
                    }
                }
            }

            var initState = stateNumByStateName[$"{ks.InitStates.First().Name},{ltl.InitStates.First().Name}"];

            return new SynchronousComposition(stateNumByStateName, initState, scEdges);
        }
    }
}