﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Web.Contract.Models;
using static TemporalLogicVerifier.Web.Contract.Mapping.CTLMapping;

namespace TemporalLogicVerifier.Web.Generators
{
    public class KripkeStructureGenerator : IKripkeStructureGenerator
    {
        private readonly int _statesNum;
        private readonly int _initialStatesNum;
        private readonly Random _rand;
        private readonly int _markedStatesNum;
        private readonly int _maxPredicatesNumInOneState;

        private readonly IKripkeStructureFactory _kripkeStructureFactory;

        public KripkeStructureGenerator(IKripkeStructureFactory kripkeStructureFactory)
        {
            _kripkeStructureFactory =
                kripkeStructureFactory ?? throw new ArgumentNullException(nameof(kripkeStructureFactory));

            _statesNum = 6;
            _initialStatesNum = 1;
            _rand = new Random();
            _markedStatesNum = 4;
            _maxPredicatesNumInOneState = 2;
        }

        public IKripkeStructure InitRandom(IReadOnlyList<ICTLFormula> atomicPredicates)
        {
            var states = GenerateStates();
            var initialStates = GenerateInitialStates(states);
            var totalRatio = GenerateTotalRatio(states);
            var marksFunction = GenerateMarksFunction(states, atomicPredicates);

            var kripkeStructure = _kripkeStructureFactory
                .CreateKripkeStructure(
                    states: states,
                    initialStates: initialStates,
                    totalRatio: totalRatio,
                    atomicPredicates: atomicPredicates,
                    markFunction: marksFunction);

            return kripkeStructure;

            //_formulaCTL = GenerateFormulaCTL(*atomicPredicates);
            //_kripkeStructure->ModelCheck(_formulaCTL);
        }

        public ILtlKripkeStructure InitRandom(IReadOnlyList<ILTLFormula> atomicPredicates)
        {
            var states = GenerateStates();
            var initialStates = GenerateInitialStates(states);
            var totalRatio = GenerateTotalRatio(states);
            var marksFunction = GenerateMarksFunction(states, atomicPredicates);

            var ltlKripkeStructure = _kripkeStructureFactory
                .CreateKripkeStructure(
                    states: states,
                    initialStates: initialStates,
                    totalRatio: totalRatio,
                    atomicPredicates: atomicPredicates,
                    markFunction: marksFunction);

            return ltlKripkeStructure;
        }

        public IState GetCheckingState(IKripkeStructure kripkeStructure)
        {
            var state = kripkeStructure.States;
            var stateNum = _rand.Next() % state.Count;

            return state[stateNum];
        }

        private IReadOnlyList<IState> GenerateStates()
        {
            var nameState = "S";
            var states = new List<IState>();

            for (var i = 0; i < _statesNum; i++)
            {
                states.Add(_kripkeStructureFactory.CreateState($"{nameState}{i}"));
            }

            return states;
        }

        private IReadOnlyList<IState> GenerateInitialStates(IReadOnlyList<IState> states)
        {
            var initialStates = new List<IState>();

            for (var currentInitialStateNum = 0; currentInitialStateNum < _initialStatesNum; currentInitialStateNum++)
            {
                var stateNum = _rand.Next(1, int.MaxValue) % states.Count();

                initialStates.Add(states[stateNum]);
            }

            return initialStates;
        }

        private IReadOnlyList<ITotalRatio> GenerateTotalRatio(IReadOnlyList<IState> states)
        {
            var totalRatio = new List<ITotalRatio>();

            for (var currentEdgeNum = 0; currentEdgeNum < 14; currentEdgeNum++)
            {
                var leftStateNum = GenerateRandStateNum();
                var rightStateNum = GenerateRandStateNum();

                var leftState = states[leftStateNum];
                var rightState = states[rightStateNum];

                var edge = _kripkeStructureFactory.CreteTotalRatio(leftState, rightState);

                var edgeExists = false;
                foreach (var ratio in totalRatio)
                {
                    if (ratio.FirstState == leftState && ratio.SecondState == rightState)
                    {
                        edgeExists = true;
                        break;
                    }
                }

                if (edgeExists)
                {
                    currentEdgeNum--;
                    continue;
                }

                totalRatio.Add(edge);
            }

            //TO DO:: !!need check connectivity!!!!
            return totalRatio;
        }

        private int GenerateRandStateNum()
        {
            return _rand.Next() % _statesNum;
        }

        private IDictionary<IState, IList<ICTLFormula>> GenerateMarksFunction(
            IReadOnlyList<IState> states,
            IReadOnlyList<ICTLFormula> atomicPredicates)
        {
            var marksFunction = new Dictionary<IState, IList<ICTLFormula>>();

            for (var stateNum = 0; stateNum < _markedStatesNum; stateNum++)
            {
                var randStateNum = GenerateRandStateNum();
                var state = states[randStateNum];

                if (marksFunction.TryGetValue(state, out var formulas))
                {
                    stateNum--;
                    continue;
                }

                var atomicPredicatesCountInTheState =
                    (_rand.Next(0, int.MaxValue) % _maxPredicatesNumInOneState + 1) % atomicPredicates.Count;
                var atomicPredicatesInTheState = new List<ICTLFormula>();

                for (var atomicPredicateNum = 0;
                    atomicPredicateNum < atomicPredicatesCountInTheState;
                    atomicPredicateNum++)
                {
                    var randAtomicPredicateNum = _rand.Next(1, int.MaxValue) % atomicPredicates.Count - 1;
                    var randAtomicPredicate = atomicPredicates[atomicPredicateNum];

                    if (atomicPredicatesInTheState.Contains(randAtomicPredicate))
                    {
                        atomicPredicateNum--;
                        continue;
                    }

                    atomicPredicatesInTheState.Add(randAtomicPredicate);
                }

                marksFunction[state] = atomicPredicatesInTheState;
            }


            if (!marksFunction.Values.Any(x => x.Any()))
            {
                var randStateNum = GenerateRandStateNum();
                var state = states[randStateNum];
                marksFunction[state] = new List<ICTLFormula>() {atomicPredicates[0]};
            }

            return marksFunction;
        }
        private IDictionary<IState, IList<ILTLFormula>> GenerateMarksFunction(
            IReadOnlyList<IState> states,
            IReadOnlyList<ILTLFormula> atomicPredicates)
        {
            var marksFunction = new Dictionary<IState, IList<ILTLFormula>>();

            for (var stateNum = 0; stateNum < _markedStatesNum; stateNum++)
            {
                var randStateNum = GenerateRandStateNum();
                var state = states[randStateNum];

                if (marksFunction.TryGetValue(state, out var formulas))
                {
                    stateNum--;
                    continue;
                }

                var atomicPredicatesCountInTheState =
                    (_rand.Next(0, int.MaxValue) % _maxPredicatesNumInOneState + 1) % atomicPredicates.Count;
                var atomicPredicatesInTheState = new List<ILTLFormula>();

                for (var atomicPredicateNum = 0;
                    atomicPredicateNum < atomicPredicatesCountInTheState;
                    atomicPredicateNum++)
                {
                    var randAtomicPredicateNum = _rand.Next(1, int.MaxValue) % atomicPredicates.Count - 1;
                    var randAtomicPredicate = atomicPredicates[atomicPredicateNum];

                    if (atomicPredicatesInTheState.Contains(randAtomicPredicate))
                    {
                        atomicPredicateNum--;
                        continue;
                    }

                    atomicPredicatesInTheState.Add(randAtomicPredicate);
                }

                marksFunction[state] = atomicPredicatesInTheState;
            }


            if (!marksFunction.Values.Any(x => x.Any()))
            {
                var randStateNum = GenerateRandStateNum();
                var state = states[randStateNum];
                marksFunction[state] = new List<ILTLFormula>() { atomicPredicates[0] };
            }

            return marksFunction;
        }
        /*public bool CheckMarkedsStates(string userId, IState state, IReadOnlyList<ICTLFormula> formulas)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }

            //TODO: Add comparing for UserInfo class
            if (KripkeStructureStorage.Keys.All(x => x.UserId != userId))
            {
                throw new Exception($"\"{userId}\" user doesn't exist");
            }

            var currentUser = GetUserById(userId);
            if (!currentUser.ModelCheckingWasRunning)
            {
                if (CTLFormulaService.CTLFormulaStorage.Keys.All(x => x.UserId != userId))
                {
                    throw new Exception("CTL formula wasn't generated");
                }

                var formula =
                    CTLFormulaService.CTLFormulaStorage[
                        CTLFormulaService.CTLFormulaStorage.Keys.First(x => x.UserId == userId)];

                KripkeStructureStorage[currentUser].ModelChecker(formula);
                currentUser.ModelCheckingWasRunning = true;
            }

            return CheckMarkedsStates(
                KripkeStructureStorage[currentUser],
                state,
                formulas);
        }

        private bool CheckMarkedsStates(
            IKripkeStructure kripkeStructure,
            IState state,
            IReadOnlyList<ICTLFormula> formulas)
        {
            var ksState = kripkeStructure.States.FirstOrDefault(x => x.Name == state.Name);
            if (ksState == null)
            {
                throw new Exception($"\"{state.Name}\" state insn't contained in the current kripke structure");
            }

            var markedFormulas = kripkeStructure.MarkFunction(ksState);

            if ((markedFormulas == null || !markedFormulas.Any()) && (formulas == null || !formulas.Any()))
            {
                return true;
            }

            var formulasContainins = formulas.ToDictionary(x => x, x => false);
            foreach (var formula in markedFormulas)
            {
                var f = formulas.FirstOrDefault(x => !x.Equals(formula));
                if (f == null)
                {
                    return false;
                }
                formulasContainins[f] = true;
            }

            if (formulasContainins.Values.Contains(false))
            {
                return false;
            }

            return true;
        }*/
    }
}