﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.CTLFormula;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Web.Contract.Models;
using static TemporalLogicVerifier.Web.Contract.Mapping.CTLMapping;

namespace TemporalLogicVerifier.Web.Generators
{
    public class CtlFormulaGenerator : ICtlFormulaGenerator
    {
        /*private readonly int _statesNum;
        private readonly int _initialStatesNum;
        private readonly int _edgesNum;
        private readonly int _markedStatesNum;
        private readonly int _maxPredicatesNumInOneState;
        private readonly Random _rand;*/
        private readonly int _maxFormulaDeep = 6;
        private readonly int _maxPredicatesNum = 3;
        private IReadOnlyList<ICTLFormula> _atomicPredicates;
        private readonly Random _random;

        private readonly ICTLFormulaFactory _ctlFormulaFactory;

        public CtlFormulaGenerator(ICTLFormulaFactory ctlFormulaFactory)
        {
            _ctlFormulaFactory = ctlFormulaFactory ?? throw new ArgumentNullException(nameof(ctlFormulaFactory));
            _random = new Random();
            _atomicPredicates = GenerateAtomicPredicates();

            /*_statesNum = 9;
            _initialStatesNum = 2;
            _edgesNum = 9;
            _markedStatesNum = 7;
            _maxPredicatesNumInOneState = 2;
            _rand = new Random();*/
        }

        public ICTLFormula InitRandom()
        {
            /*var p = _atomicPredicates[0];
            var q = _atomicPredicates[1];
            var r = _atomicPredicates[2];

            var non_p = _ctlFormulaFactory.CreateNegativeFormula(p);
            var q_or_r = _ctlFormulaFactory.CreateOr(q, r);
            var ex_non_p = _ctlFormulaFactory.CreateEX(non_p);
            var af_q_or_r = _ctlFormulaFactory.CreateAF(q_or_r);
            var formula = _ctlFormulaFactory.CreateOr(ex_non_p, af_q_or_r);*/
            
            var formulaDeep = 2 + _random.Next() % _maxFormulaDeep;
            var formula = GenerateFormulaRecursively(formulaDeep, 1);

            return formula;
        }

        public IReadOnlyList<ICTLFormula> GetAtomicPredicatesUsedByFormula(ICTLFormula ctlFormula)
        {
            IList<ICTLFormula> subFormulas = new List<ICTLFormula>();
            GetSubFormulas(ctlFormula, ref subFormulas);

            var usingPredicates = new List<ICTLFormula>();
            foreach (var subFormula in subFormulas)
            {
                if (subFormula is AtomicPredicate ap)
                {
                    if (!usingPredicates.Any() || usingPredicates.All(x => !x.Equals(ap)))
                    {
                        usingPredicates.Add(ap);
                    }
                }
            }

            return usingPredicates;
        }

        private static void GetSubFormulas(ICTLFormula formula, ref IList<ICTLFormula> subFormulas)
        {
            if (formula.Left != null)
            {
                GetSubFormulas(formula.Left, ref subFormulas);
            }

            if (formula.Right != null)
            {
                GetSubFormulas(formula.Right, ref subFormulas);
            }

            subFormulas.Add(formula);
        }

        private IReadOnlyList<ICTLFormula> GenerateAtomicPredicates()
        {
            var atomicPredicates = new List<ICTLFormula>();
            var namesAtomicPredicates = new char[7] { 'p', 'q', 't', 'r', 'z', 'w', 'g' };

            var predicateCount = _random.Next() % _maxPredicatesNum;
            if (predicateCount == 0)
            {
                predicateCount = 2;
            }
            
            for (var predicateNum = 0; predicateNum < predicateCount; predicateNum++)
            {
                atomicPredicates.Add(_ctlFormulaFactory.CreateAtomicPredicate($"{namesAtomicPredicates[predicateNum]}"));
            }
            return atomicPredicates;
        }

        private ICTLFormula GenerateFormulaRecursively(
            int currentFormulaDeep,
            int recursiveLevel)
        {
            if (currentFormulaDeep == 1 || recursiveLevel==4)
            {
                return _atomicPredicates[_random.Next() % _atomicPredicates.Count];
            }

            currentFormulaDeep--;
            recursiveLevel++;
            return _ctlFormulaFactory.CreateRandomFormula(
                GenerateFormulaRecursively(currentFormulaDeep, recursiveLevel),
                GenerateFormulaRecursively(currentFormulaDeep, recursiveLevel));
        }


        /*public IKripkeStructure GetKripkeStructure()
        {
            var states = GenerateStates();
            var initialStates = GenerateInitialStates(states);
            var totalRatio = GenerateTotalRatio(states);
            var atomicPredicates = GenerateAtomicPredicates();
            var marksFunction = GenerateMarksFunction(states, atomicPredicates);

            return
                new KripkeStructure.KripkeStructure(
                    states: states,
                    initialStates: initialStates,
                    totalRatio: totalRatio,
                    atomicPredicates: atomicPredicates,
                    markFunction: marksFunction);

            //_formulaCTL = GenerateFormulaCTL(*atomicPredicates);
            //_kripkeStructure->ModelCheck(_formulaCTL);
        }*/

        /*
        private IReadOnlyList<IState> GenerateStates()
        {
            var nameState = "S_";
            var states = new List<IState>();

            for (var i = 0; i < _statesNum; i++)
            {
                states.Add(new State($"{nameState}_{i}"));
            }

            return states;
        }

        private IReadOnlyList<IState> GenerateInitialStates(IReadOnlyList<IState> states)
        {
            var initialStates = new List<IState>();

            for (var currentInitialStateNum = 0; currentInitialStateNum < _initialStatesNum; currentInitialStateNum++)
            {
                var stateNum = _rand.Next(1, int.MaxValue) % states.Count() - 1;
                initialStates.Add(states[stateNum]);
            }

            return initialStates;
        }

        private IReadOnlyList<ITotalRatio> GenerateTotalRatio(IReadOnlyList<IState> states)
        {
            var totalRatio = new List<ITotalRatio>();

            for (var currentEdgeNum = 0; currentEdgeNum < 14; currentEdgeNum++)
            {
                var leftStateNum = GenerateRandStateNum();
                var rightStateNum = GenerateRandStateNum();

                var leftState = states[leftStateNum];
                var rightState = states[rightStateNum];

                var edge = new TotalRatio(leftState, rightState);

                var edgeExists = false;
                foreach (var ratio in totalRatio)
                {
                    if (ratio.FirstState == leftState && ratio.SecondState == rightState)
                    {
                        edgeExists = true;
                        break;
                    }
                }

                if (edgeExists)
                {
                    currentEdgeNum--;
                    continue;
                }

                totalRatio.Add(edge);
            }

            //TO DO:: !!need check connectivity!!!!
            return totalRatio;
        }

        private int GenerateRandStateNum()
        {
            var randStateNum = _rand.Next(1, int.MaxValue) % _statesNum;
            if (randStateNum < _statesNum)
            {
                return randStateNum;
            }

            return GenerateRandStateNum();
        }

        private IDictionary<IState, IList<ICTLFormula>> GenerateMarksFunction(
            IReadOnlyList<IState> states,
            IReadOnlyList<ICTLFormula> atomicPredicates)
        {
            var marksFunction = new Dictionary<IState, IList<ICTLFormula>>();

            for (var stateNum = 0; stateNum < _markedStatesNum; stateNum++)
            {
                var randStateNum = GenerateRandStateNum();
                var state = states[randStateNum];

                if (marksFunction.TryGetValue(state, out var formulas))
                {
                    stateNum--;
                    continue;
                }

                var atomicPredicatesCountInTheState = _rand.Next(0, int.MaxValue) % _maxPredicatesNumInOneState + 1;
                var atomicPredicatesInTheState = new List<ICTLFormula>();

                for (var atomicPredicateNum = 0;
                    atomicPredicateNum < atomicPredicatesCountInTheState;
                    atomicPredicateNum++)
                {
                    var randAtomicPredicateNum = _rand.Next(1, int.MaxValue) % _predicatesNum - 1;
                    var randAtomicPredicate = atomicPredicates[atomicPredicateNum];

                    if (atomicPredicatesInTheState.Contains(randAtomicPredicate))
                    {
                        atomicPredicateNum--;
                        continue;
                    }

                    atomicPredicatesInTheState.Add(randAtomicPredicate);
                }

                marksFunction[state] = atomicPredicatesInTheState;
            }

            return marksFunction;
        }*/
    }
}