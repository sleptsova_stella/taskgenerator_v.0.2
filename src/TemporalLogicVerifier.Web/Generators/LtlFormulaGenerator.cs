﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.Web.Contract.Models;
using TemporalLogicVerifier.LTLFormula;
using static TemporalLogicVerifier.Web.Contract.Mapping.CTLMapping;

namespace TemporalLogicVerifier.Web.Generators
{
    public class LtlFormulaGenerator : ILtlFormulaGenerator
    {
        private readonly int _maxFormulaDeep = 2;
        private readonly int _maxPredicatesNum = 3;
        private IReadOnlyList<ILTLFormula> _atomicPredicates;
        private readonly Random _random;

        private readonly IDictionary<int, ILTLFormula> _ltlFormulas
            = new Dictionary<int, ILTLFormula>()
        {
            { 0, new And(new G(_p), new G(_q))},
            {1, new F(new And(_p,new X(new G(new NegativeFormula(_q)))))},
            { 2, new F(new G(_p))},
            { 3,new G(new Implication(_p,new X(_q)))},
            { 4, new G(new Implication(_p,new Or(new X(_q), new X(new X(_q)))))},
            { 5, new F(_p)},
            { 6, new G(_p)},
            { 7,new G(new F(_p))},
            { 8, new G(new Implication(_p,new G(_p)))},
            { 9,new U(_p,_q)},
            { 10, new U(new Or(_p,_q), new And(_p,_q))},
            { 11,new F(new G(_p))}
        };

        private readonly ILTLFormulaFactory _ltlFormulaFactory;


        private static ILTLFormula _p = new AtomicPredicate("p");
        private static ILTLFormula _q = new AtomicPredicate("q");

        public LtlFormulaGenerator(ILTLFormulaFactory ctlFormulaFactory)
        {
            _ltlFormulaFactory = ctlFormulaFactory ?? throw new ArgumentNullException(nameof(ctlFormulaFactory));
            _random = new Random();
            //_atomicPredicates = GenerateAtomicPredicates();
            _atomicPredicates = new ILTLFormula[2] {_p, _q};
        }

        public ILTLFormula InitRandom()
        {
            /*
            var formulaDeep = 2 + _random.Next() % _maxFormulaDeep;
            var formula = GenerateFormulaRecursively(formulaDeep, 1);
            */

            var i = _random.Next() % 12;
            //return _ltlFormulas[i];
            return new And(new G(_p), new G(_q));
        }

        public IReadOnlyList<ILTLFormula> GetAtomicPredicatesUsedByFormula(ILTLFormula ctlFormula)
        {
            IList<ILTLFormula> subFormulas = new List<ILTLFormula>();
            GetSubFormulas(ctlFormula, ref subFormulas);

            var usingPredicates = new List<ILTLFormula>();
            foreach (var subFormula in subFormulas)
            {
                if (subFormula is AtomicPredicate ap)
                {
                    if (!usingPredicates.Any() || usingPredicates.All(x => !x.Equals(ap)))
                    {
                        usingPredicates.Add(ap);
                    }
                }
            }

            return usingPredicates;
        }

        private static void GetSubFormulas(ILTLFormula formula, ref IList<ILTLFormula> subFormulas)
        {
            if (formula.Left != null)
            {
                GetSubFormulas(formula.Left, ref subFormulas);
            }

            if (formula.Right != null)
            {
                GetSubFormulas(formula.Right, ref subFormulas);
            }

            subFormulas.Add(formula);
        }

        private IReadOnlyList<ILTLFormula> GenerateAtomicPredicates()
        {
            var atomicPredicates = new List<ILTLFormula>();
            var namesAtomicPredicates = new char[7] {'p', 'q', 't', 'r', 'z', 'w', 'g'};

            var predicateCount = _random.Next() % _maxPredicatesNum;
            if (predicateCount == 0)
            {
                predicateCount = 2;
            }

            for (var predicateNum = 0; predicateNum < predicateCount; predicateNum++)
            {
                atomicPredicates.Add(
                    _ltlFormulaFactory.CreateAtomicPredicate($"{namesAtomicPredicates[predicateNum]}"));
            }

            return atomicPredicates;
        }

        private ILTLFormula GenerateFormulaRecursively(
            int currentFormulaDeep,
            int recursiveLevel)
        {
            if (currentFormulaDeep == 1 || recursiveLevel == 4)
            {
                return _atomicPredicates[_random.Next() % _atomicPredicates.Count];
            }

            currentFormulaDeep--;
            recursiveLevel++;
            return _ltlFormulaFactory.CreateRandomFormula(
                GenerateFormulaRecursively(currentFormulaDeep, recursiveLevel),
                GenerateFormulaRecursively(currentFormulaDeep, recursiveLevel));
        }
    }
}