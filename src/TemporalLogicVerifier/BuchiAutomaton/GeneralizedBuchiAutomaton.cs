﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.BuchiAutomaton
{
    public class GeneralizedBuchiAutomaton : IGeneralizedBuchiAutomaton
    {
        public IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> EdgeMarker { get; }

        /// <summary>
        /// Value state is atom (list of LTL formula)
        /// </summary>
        public IDictionary<IState, IReadOnlyList<ILTLFormula>> StateMarker { get; }

        public IReadOnlyList<IReadOnlyList<ILTLFormula>> Alphabet { get; }

        public IReadOnlyList<IState> States { get; }

        public IReadOnlyList<IState> InitStates { get; }

        /// <summary>
        /// При пустом множестве F квантор Any(Fi \in F) становится истинным.
        /// Поэтому любая бесконечная цепочка в пстроенном автомате Бюхи допускается.
        /// </summary>
        public IReadOnlyList<IReadOnlyList<IState>> FinalStates { get; }

        public IReadOnlyList<ILTLFormula> EdgeValue(ITotalRatio ratio)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (!EdgeMarker.ContainsKey(ratio))
            {
                return null;
            }

            return EdgeMarker[ratio];
        }

        public IReadOnlyList<ILTLFormula> EdgeValue(IState state1, IState state2)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (EdgeMarker.Keys.FirstOrDefault(x => x.FirstState == state1 && x.SecondState == state2) == null)
            {
                return null;
            }

            var key = EdgeMarker.Keys.First(
                x => x.FirstState == state1 &&
                     x.SecondState == state2);

            return EdgeMarker[key];
        }

        public IReadOnlyList<ILTLFormula> StateValue(IState state)
        {
            if (!States.Contains(state))
            {
                throw new Exception($"ERROR - BuchiAutomatonByLTLFormula: state \"{state.Name}\" isn't contained in States");
            }

            if (!StateMarker.ContainsKey(state))
            {
                return null;
            }

            return StateMarker[state];
        }

        public GeneralizedBuchiAutomaton(
                IReadOnlyList<IReadOnlyList<ILTLFormula>> alphabet,
                IReadOnlyList<IState> states,
                IReadOnlyList<IState> initStates,
                IReadOnlyList<IReadOnlyList<IState>> finalStates,
                IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> edgeMarker,
                IDictionary<IState, IReadOnlyList<ILTLFormula>> stateMarker)
        {
            Alphabet = alphabet ?? throw new ArgumentNullException(nameof(alphabet));
            States = states ?? throw new ArgumentNullException(nameof(states));
            InitStates = initStates ?? throw new ArgumentNullException(nameof(initStates));
            FinalStates = finalStates ?? throw new ArgumentNullException(nameof(finalStates));
            EdgeMarker = edgeMarker ?? throw new ArgumentNullException(nameof(edgeMarker));
            StateMarker = stateMarker ?? throw new ArgumentNullException(nameof(stateMarker));
        }
    }
}
