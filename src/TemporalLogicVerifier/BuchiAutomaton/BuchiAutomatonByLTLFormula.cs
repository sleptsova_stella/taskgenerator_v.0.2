﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.BuchiAutomaton
{
    public class BuchiAutomatonByLTLFormula : IBuchiAutomatonByLTLFormula
    {
        private IDictionary<ITotalRatio, IReadOnlyList<IReadOnlyList<ILTLFormula>>> _edgeValue;

        /// <summary>
        /// Value state is atom (list of LTL formula)
        /// </summary>
        private IDictionary<IState, IReadOnlyList<ILTLFormula>> _stateValue;

        public IReadOnlyList<IReadOnlyList<ILTLFormula>> Alphabet { get; }

        public IReadOnlyList<IState> States { get; }

        public IReadOnlyList<IState> InitStates { get; }

        public IReadOnlyList<IState> FinalStates { get; }

        public IReadOnlyList<IReadOnlyList<ILTLFormula>> EdgeValue(ITotalRatio ratio)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (!_edgeValue.ContainsKey(ratio))
            {
                return null;
            }

            return _edgeValue[ratio];
        }

        public IReadOnlyList<IReadOnlyList<ILTLFormula>> EdgeValue(IState state1, IState state2)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (_edgeValue.Keys.FirstOrDefault(x => x.FirstState == state1 && x.SecondState == state2) == null)
            {
                return null;
            }

            var key = _edgeValue.Keys.First(
                x => x.FirstState == state1 &&
                     x.SecondState == state2);

            return _edgeValue[key];
        }

        public IReadOnlyList<ILTLFormula> StateValue(IState state)
        {
            if (!States.Contains(state))
            {
                throw new Exception($"ERROR - BuchiAutomatonByLTLFormula: state \"{state.Name}\" isn't contained in States");
            }

            if (!_stateValue.ContainsKey(state))
            {
                return null;
            }

            return _stateValue[state];
        }

        public BuchiAutomatonByLTLFormula(
            IReadOnlyList<IReadOnlyList<ILTLFormula>> alphabet,
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initStates,
            IReadOnlyList<IState> finalStates,
            IDictionary<ITotalRatio, IReadOnlyList<IReadOnlyList<ILTLFormula>>> edgeMarker,
            IDictionary<IState, IReadOnlyList<ILTLFormula>> stateMarker)
        {
            Alphabet = alphabet ?? throw new ArgumentNullException(nameof(alphabet));
            States = states ?? throw new ArgumentNullException(nameof(states));
            InitStates = initStates ?? throw new ArgumentNullException(nameof(initStates));
            FinalStates = finalStates ?? throw new ArgumentNullException(nameof(finalStates));
            _edgeValue = edgeMarker ?? throw new ArgumentNullException(nameof(edgeMarker));
            _stateValue = stateMarker ?? throw new ArgumentNullException(nameof(stateMarker));
        }
    }
}
