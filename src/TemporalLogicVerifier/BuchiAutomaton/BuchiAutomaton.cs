﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.BuchiAutomaton
{
    public class BuchiAutomaton : IBuchiAutomaton
    {
        private IDictionary<IState, IReadOnlyList<ILTLFormula>> _stateValue;
        //TODO: use readOnlyList
        public IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> EdgeMarker { get; }

        public IReadOnlyList<IReadOnlyList<ILTLFormula>> Alphabet { get; }

        public IReadOnlyList<IState> States { get; }

        public IReadOnlyList<IState> InitStates { get; }

        public IReadOnlyList<IState> FinalStates { get; }

        public IReadOnlyList<ILTLFormula> EdgeValue(ITotalRatio ratio)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (!EdgeMarker.ContainsKey(ratio))
            {
                return null;
            }

            return EdgeMarker[ratio];
        }

        public IReadOnlyList<ILTLFormula> EdgeValue(IState state1, IState state2)
        {
            //TODO: add verifier for characterFromTheAlphabet in
            if (EdgeMarker.Keys.FirstOrDefault(x => x.FirstState == state1 && x.SecondState == state2) == null)
            {
                return null;
            }

            var key = EdgeMarker.Keys.First(
                x => x.FirstState == state1 &&
                     x.SecondState == state2);

            return EdgeMarker[key];
        }

        public IReadOnlyList<ILTLFormula> StateValue(IState state)
        {
            if (!States.Contains(state))
            {
                throw new Exception($"ERROR - BuchiAutomatonByLTLFormula: state \"{state.Name}\" isn't contained in States");
            }

            if (!_stateValue.ContainsKey(state))
            {
                return null;
            }

            return _stateValue[state];
        }

        public BuchiAutomaton(
            IReadOnlyList<IReadOnlyList<ILTLFormula>> alphabet, // or IReadOnlyList<string>
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initStates,
            IReadOnlyList<IState> finalStates,
            IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> edgeMarker,
            IDictionary<IState, IReadOnlyList<ILTLFormula>> stateMarker)
        {
            Alphabet = alphabet;
            States = states;
            InitStates = initStates;
            FinalStates = finalStates;
            EdgeMarker = edgeMarker;
            _stateValue = stateMarker;
        }
    }
}
