﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    /// <summary>
    /// Выполняется в состоянии s, если из s существует путь,
    /// на котором когда-нибудь в будущем выполнится right,
    /// а до этого во всех состояниях этого пути выполняется left
    /// </summary>
    public class EU : ICTLFormula
    {
        public ICTLFormula Left { get; }

        public ICTLFormula Right { get; }

        public int DepthSize { get; }

        public bool Marked { get; set; }

        public CTLFormulaType CtlFormulaType => CTLFormulaType.EU;

        public EU(ICTLFormula left, ICTLFormula right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
            DepthSize = Math.Max(left.DepthSize, right.DepthSize) + 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"E({Left.ToString()}U{Right.ToString()})";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return Left.Equals(other.Left) && Right.Equals(other.Right);
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var X = new List<IState>();
            X.AddRange(kripkeStructure.States);

            var W = new List<IState>();
            var statesMarkedByLeft = kripkeStructure.
                States
                .Where(state => kripkeStructure
                    .MarkFunction(state)
                    .Contains(Left)
                );
            W.AddRange(statesMarkedByLeft);

            var Y = new List<IState>();
            var statesMarkedByRight = kripkeStructure.
                States
                .Where(state => kripkeStructure
                    .MarkFunction(state)
                    .Contains(Right)
                );
            Y.AddRange(statesMarkedByRight);

            while (X.Count() != Y.Count())
            {
                X.RemoveRange(0, X.Count());
                X.AddRange(Y);

                foreach (var state in kripkeStructure.States)
                {
                    if (!Y.Contains(state))
                    {
                        var children = kripkeStructure
                            .TotalRatio
                            .Where(ratio => ratio.FirstState.Equals(state))
                            .Select(ratio => ratio.SecondState)
                            .ToList();

                        if (children != null && children.Any() && children.Any(child => Y.Contains(child))
                            && W.Contains(state))
                        {
                            Y.Add(state);
                        }
                    }
                }
            }

            return Y;
        }
    }
}
