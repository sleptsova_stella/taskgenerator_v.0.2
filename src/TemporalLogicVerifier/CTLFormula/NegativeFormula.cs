﻿using System;
using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    public class NegativeFormula : ICTLFormula
    {
        public ICTLFormula Left { get; }

        public ICTLFormula Right => null;

        public CTLFormulaType CtlFormulaType => CTLFormulaType.NegativeFormula;

        public int DepthSize { get; }

        public bool Marked { get; set; }

        public NegativeFormula(ICTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
            DepthSize = formula.DepthSize + 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"!({Left.ToString()})";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return Left.Equals(other.Left);
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var resultStates = new List<IState>();
            
            foreach (var state in kripkeStructure.States)
            {
                var truePredicates = kripkeStructure.MarkFunction(state);
                if (truePredicates != null && !truePredicates.Contains(Left))
                {
                    resultStates.Add(state);
                }
            }

            return resultStates;
        }
    }
}
