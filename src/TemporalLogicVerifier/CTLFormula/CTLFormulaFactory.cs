﻿using System;
using TemporalLogicVerifier.Contract.CTLFormula;

namespace TemporalLogicVerifier.CTLFormula
{
    public class CTLFormulaFactory : ICTLFormulaFactory
    {
        public ICTLFormula CreateAtomicPredicate(string name)
        {
            return new AtomicPredicate(name);
        }

        public ICTLFormula CreateNegativeFormula(ICTLFormula subFormula)
        {
            return new NegativeFormula(subFormula);
        }

        public ICTLFormula CreateOr(ICTLFormula leftSubFormula, ICTLFormula rightSubFormula)
        {
            return new Or(leftSubFormula,rightSubFormula);
        }

        public ICTLFormula CreateAF(ICTLFormula subFormula)
        {
            return new AF(subFormula);
        }

        public ICTLFormula CreateEU(ICTLFormula leftSubFormula, ICTLFormula rightSubFormula)
        {
            return new EU(leftSubFormula,rightSubFormula);
        }

        public ICTLFormula CreateEX(ICTLFormula subFormula)
        {
            return new EX(subFormula);
        }

        public ICTLFormula CreateRandomFormula(ICTLFormula leftFormula, ICTLFormula rightFormula)
        {
            var randomFormulaNum = (new Random()).Next() % 5;

            switch (randomFormulaNum)
            {
                case 0:
                    return CreateOr(leftFormula, rightFormula);
                case 1:
                    return CreateAF(leftFormula);
                case 2:
                    return CreateEU(leftFormula, rightFormula);
                case 3:
                    return CreateNegativeFormula(leftFormula);
                case 4:
                    return CreateEX(leftFormula);
            }

            throw new Exception($"Unsupported CTL formula number - {randomFormulaNum}");
        }
    }
}