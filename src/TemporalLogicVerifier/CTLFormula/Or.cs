﻿using System;
using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    public class Or : ICTLFormula
    {
        public ICTLFormula Left { get; }

        public ICTLFormula Right { get; }

        public CTLFormulaType CtlFormulaType => CTLFormulaType.Or;

        public int DepthSize { get; }

        public bool Marked { get; set; }

        public Or(ICTLFormula left, ICTLFormula right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
            DepthSize = Math.Max(left.DepthSize, right.DepthSize) + 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"({Left.ToString()} OR {Right.ToString()})";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return Left.Equals(other.Left) && Right.Equals(other.Right);
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var resultStates = new List<IState>();

            foreach (var state in kripkeStructure.States)
            {
                var truePredicates = kripkeStructure.MarkFunction(state);
                if (truePredicates != null && (truePredicates.Contains(Left) || truePredicates.Contains(Right)))
                {
                    resultStates.Add(state);
                }
            }

            return resultStates;
        }
    }
}