﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    /// <summary>
    /// Неизбежно formula
    /// Выполняется в состоянии s, если на всех путях,
    /// начинающихся из состояния s, формула formula когда-нибудь выполнится 
    /// </summary>
    public class AF : ICTLFormula
    {
        public ICTLFormula Left { get; }

        public ICTLFormula Right => null;

        public CTLFormulaType CtlFormulaType => CTLFormulaType.AF;

        public int DepthSize { get; }

        public bool Marked { get; set; }
        
        public AF(ICTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
            DepthSize = formula.DepthSize + 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"{CtlFormulaType.ToString()}({Left.ToString()})";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return Left.Equals(other.Left);
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var X = new List<IState>();
            X.AddRange(kripkeStructure.States);

            var Y = new List<IState>();
            var statesMarkedByFormula = kripkeStructure.
                States
                .Where(state => kripkeStructure
                    .MarkFunction(state)
                    .Contains(Left)
                );
            Y.AddRange(statesMarkedByFormula);

            while (X.Count() != Y.Count())
            {
                X.RemoveRange(0, X.Count());
                X.AddRange(Y);

                foreach (var state in kripkeStructure.States)
                {
                    if (!Y.Contains(state))
                    {
                        var children = kripkeStructure
                            .TotalRatio
                            .Where(ratio => ratio.FirstState.Equals(state))
                            .Select(ratio => ratio.SecondState)
                            .ToList();

                        if (children != null && children.Any() && children.All(child => Y.Contains(child)))
                        {
                            Y.Add(state);
                        }
                    }
                }
            }

            return Y;
        }
    }
}
