﻿using System;
using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    public class AtomicPredicate : ICTLFormula
    {
        private readonly string _name;

        public ICTLFormula Left => null;

        public ICTLFormula Right => null;

        public CTLFormulaType CtlFormulaType => CTLFormulaType.AtomicPredicate;

        public int DepthSize { get; }

        public bool Marked { get; set; }

        public AtomicPredicate(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            _name = name;
            DepthSize = 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"{_name}";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return _name == ((AtomicPredicate)other)._name;
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var resultStates = new List<IState>();

            foreach (var state in kripkeStructure.States)
            {
                var truePredicates = kripkeStructure.MarkFunction(state);
                if (truePredicates != null && truePredicates.Contains(this))
                {
                    resultStates.Add(state);
                }
            }

            return resultStates;
        }
    }


}
