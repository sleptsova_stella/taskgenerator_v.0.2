﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.CTLFormula
{
    /// <summary>
    /// Выполняется в состоянии s, если формула formula выполняется хотя бы в одном состоянии,
    /// непосредственно следующем за s
    /// </summary>
    public class EX : ICTLFormula
    {
        public ICTLFormula Left { get; }

        public ICTLFormula Right => null;

        public CTLFormulaType CtlFormulaType => CTLFormulaType.EX;

        public int DepthSize { get; }

        public bool Marked { get; set; }

        public EX(ICTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
            DepthSize = formula.DepthSize + 1;
            Marked = false;
        }

        public override string ToString()
        {
            return $"EX({Left.ToString()})";
        }

        public override bool Equals(object obj)
        {
            var other = (ICTLFormula)obj;
            if (other.CtlFormulaType != CtlFormulaType)
            {
                return false;
            }

            return Left.Equals(other.Left);
        }

        public IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure)
        {
            var resultStates = new List<IState>();

            foreach (var state in kripkeStructure.States)
            {
                var children = kripkeStructure
                    .TotalRatio
                    .Where(ratio => ratio.FirstState.Equals(state))
                    .Select(ratio => ratio.SecondState);

                if (children.Any(child => kripkeStructure.MarkFunction(child).Contains(Left)))
                {
                    resultStates.Add(state);
                }
            }

            return resultStates;
        }
    }
}
