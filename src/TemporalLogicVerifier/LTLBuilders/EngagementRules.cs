﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLBuilders
{
    public static class EngagementRules
    {
        public static bool RuleC1IsFeasibly(
            IReadOnlyList<ILTLFormula> atom1,
            IReadOnlyList<ILTLFormula> atom2,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allXFormulas = allSubFormulas.Where(x => x.Type == LTLFormulaType.X);

            if (allXFormulas == null || !allXFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleC1IsFeasibly: set of formula doesn't contain X");
            }

            foreach (var x in allXFormulas)
            {
                if ((atom1.Contains(x) && !atom2.Contains(x.Left))
                    || (!atom1.Contains(x) && atom2.Contains(x.Left)))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool RuleC2IsFeasibly(
            IReadOnlyList<ILTLFormula> atom1,
            IReadOnlyList<ILTLFormula> atom2,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allUFormulas = allSubFormulas.Where(x => x.Type == LTLFormulaType.U);

            if (allUFormulas == null || !allUFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleC2IsFeasibly: set of formula doesn't contain U");
            }

            foreach (var u in allUFormulas)
            {
                if ((atom1.Contains(u) && (!atom1.Contains(u.Right) && (!atom1.Contains(u.Left) || !atom2.Contains(u))))
                    || (!atom1.Contains(u) && (atom1.Contains(u.Right) || (atom1.Contains(u.Left) && atom2.Contains(u)))))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool RuleC3IsFeasibly(
            IReadOnlyList<ILTLFormula> atom1,
            IReadOnlyList<ILTLFormula> atom2,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allFFormulas = allSubFormulas.Where(x => x.Type == LTLFormulaType.F);

            if (allFFormulas == null || !allFFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleC3IsFeasibly: set of formula doesn't contain F");
            }

            foreach (var f in allFFormulas)
            {
                if ((atom1.Contains(f) && !atom1.Contains(f.Left) && !atom2.Contains(f))
                    || (!atom1.Contains(f) && (atom1.Contains(f.Left) || atom2.Contains(f)) ))
                {
                    return false;
                }
            }

            return true;
        }

        public static bool RuleC4IsFeasibly(
            IReadOnlyList<ILTLFormula> atom1,
            IReadOnlyList<ILTLFormula> atom2,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allGFormulas = allSubFormulas.Where(x => x.Type == LTLFormulaType.G);

            if (allGFormulas == null || !allGFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleC1IsFeasibly: set of formula doesn't contain X");
            }

            foreach (var g in allGFormulas)
            {
                if ((atom1.Contains(g) && !atom2.Contains(g))
                    || (!atom1.Contains(g) && atom2.Contains(g)))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
