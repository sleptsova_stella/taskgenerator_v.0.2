﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TemporalLogicVerifier.BuchiAutomaton;
using TemporalLogicVerifier.Common;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.KripkeStructure;
using TemporalLogicVerifier.LTLFormula;

namespace TemporalLogicVerifier.LTLBuilders
{
    public static class BuchiAutomatonBuilder
    {

        public static (bool, IReadOnlyList<IReadOnlyList<IState>>) ModelCheck(IBuchiAutomatonByLTLFormula buchiAutomatonByLtlFormula,
            IBuchiAutomaton buchiAutomatonByKripkeStructure)
        {
            var buchiMachinesComposition = BuildBuchiMachinesComposition(buchiAutomatonByLtlFormula,
                buchiAutomatonByKripkeStructure);

            var cycles = buchiMachinesComposition.FindAllCicles();

            var counterExamples = new List<IReadOnlyList<IState>>();
            foreach (var cycle in cycles)
            {
                if(cycle.Any(x=>buchiAutomatonByLtlFormula.FinalStates.Contains(x)))
                {
                    counterExamples.Add(cycle);
                }
            }

            if (counterExamples.Any())
            {
                return (true, counterExamples);
            }

            return (false, null);
        }

        private static BuchiMachinesComposition BuildBuchiMachinesComposition(
            IBuchiAutomatonByLTLFormula buchiAutomatonByLtlFormula,
            IBuchiAutomaton buchiAutomatonByKripkeStructure)
        {
            return new BuchiMachinesComposition();
        }

        private class BuchiMachinesComposition
        {
            public IReadOnlyList<IReadOnlyList<IState>> FindAllCicles()
            {
                return new[] { new []{new State("1")}};
            }
        }

        private static ILTLFormula _p = new AtomicPredicate("p");
        private static ILTLFormula _q = new AtomicPredicate("q");

        public static IBuchiAutomaton Build(ILtlKripkeStructure kripkeStructure)
        {
            var ltlPreficates = kripkeStructure.AtomicPredicates
                .Select(x => new LTLFormula.AtomicPredicate(x.ToString()))
                .ToArray();

            var setOfSubSets = AlgorithmsHelper.GetAllSubSets(ltlPreficates);
            var edgeMarker = new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>();

            foreach (var ratio in kripkeStructure.TotalRatio)
            {
                var edgeValue = kripkeStructure.MarkFunction(ratio.FirstState)
                    //TODO: too much converting (use dictinary)
                    .Select(x => ltlPreficates.First(l => l.Name == (x.ToString())))
                    .ToArray();

                edgeMarker.Add(ratio, edgeValue);
            }
            
            return new BuchiAutomaton.BuchiAutomaton(
                states: kripkeStructure.States,
                initStates: kripkeStructure.InitialStates,
                alphabet: setOfSubSets,
                finalStates: kripkeStructure.States,
                edgeMarker: edgeMarker,
                stateMarker: null);
        }

        public static IBuchiAutomaton Build(ILTLFormula formula)
        {
            var random = new Random(DateTime.UtcNow.Second);
            var i = random.Next() % 4;

            var ltls = new Dictionary<int, IBuchiAutomaton>()
           {
                //Fp
                { 0, new BuchiAutomaton.BuchiAutomaton(
                    null,
                    new []{new State("1"), new State("2")},
                    new []{new State("1")},
                    new []{new State("2")},
                    new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>()
                    {
                        { new TotalRatio(new State("1"),new State("2")), new []{_p}},
                        { new TotalRatio(new State("1"),new State("1")), null},
                        { new TotalRatio(new State("2"),new State("2")), null},
                        { new TotalRatio(new State("2"),new State("2")), new []{_p}},
                    },
                    null
                    )
                },
                //Gp
                { 1, new BuchiAutomaton.BuchiAutomaton(
                        null,
                        new []{new State("1"), new State("2")},
                        new []{new State("1")},
                        new []{new State("1")},
                        new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>()
                        {
                            { new TotalRatio(new State("1"),new State("2")), null},
                            { new TotalRatio(new State("1"),new State("1")),  new []{_p}},
                            { new TotalRatio(new State("2"),new State("2")), null},
                            { new TotalRatio(new State("2"),new State("2")), new []{_p}},
                        },
                        null
                    )
                },
                //GFp
                { 2, new BuchiAutomaton.BuchiAutomaton(
                        null,
                        new []{new State("1"), new State("2")},
                        new []{new State("1")},
                        new []{new State("1")},
                        new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>()
                        {
                            { new TotalRatio(new State("1"),new State("2")), null},
                            { new TotalRatio(new State("2"),new State("1")),  new []{_p}},
                            { new TotalRatio(new State("1"),new State("1")),  new []{_p}},
                            { new TotalRatio(new State("2"),new State("2")), null},
                        },
                        null
                    )
                },
                //G(p->Gp)
                { 3, new BuchiAutomaton.BuchiAutomaton(
                        null,
                        new []{new State("1"), new State("2")},
                        new []{new State("1")},
                        new []{new State("1"), new State("2") },
                        new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>()
                        {
                            { new TotalRatio(new State("1"),new State("2")), new []{_p}},
                            { new TotalRatio(new State("1"),new State("1")), null},
                            { new TotalRatio(new State("2"),new State("2")), new []{_p}},
                        },
                        null
                    )
                }
           };
            return ltls[i];
            /*
            var generalizedBuchiAutomaton = BuildGeneralizedBuchiAutomaton(formula);

            if (generalizedBuchiAutomaton.FinalStates.Count() == 1)
            {
                return new BuchiAutomaton.BuchiAutomaton(
                    alphabet: generalizedBuchiAutomaton.Alphabet,
                    states: generalizedBuchiAutomaton.States,
                    initStates: generalizedBuchiAutomaton.InitStates,
                    finalStates: generalizedBuchiAutomaton.FinalStates.First(),
                    edgeMarker: generalizedBuchiAutomaton.EdgeMarker,
                    stateMarker: generalizedBuchiAutomaton.StateMarker);
            }

            //TODO: add new checker: if final state count == 2 (from book )

            return BuildGeneralizedBuchiAutomaton(generalizedBuchiAutomaton);*/
        }

        private static IGeneralizedBuchiAutomaton BuildGeneralizedBuchiAutomaton(ILTLFormula formula)
        {
            var alphabet = BuildAlphabet(formula).ToArray();
            var atomsByState = BuildState(formula);
            var initState = atomsByState
                .Where(atomByKey => atomByKey.Value.Contains(formula))
                .Select(x => x.Key)
                .ToArray();
            var allSubFormulas = AlgorithmsHelper.GetAllSubFormulas(formula).ToArray();
            var edgeMarger = BuildEdgeMarker(atomsByState, alphabet, allSubFormulas);
            if (edgeMarger.Count > 20)
            {
                throw new Exception();
            }
            var unreachableStates = GetUnreachableStates(initState, atomsByState, edgeMarger);
            var finalStates = BuildFinalStates(atomsByState, allSubFormulas, unreachableStates);

            return new GeneralizedBuchiAutomaton(
                alphabet: alphabet,
                states: atomsByState.Keys.ToArray(),
                initStates: initState,
                finalStates: finalStates,
                edgeMarker: edgeMarger,
                stateMarker: atomsByState);
        }

        private static IBuchiAutomaton BuildGeneralizedBuchiAutomaton(IGeneralizedBuchiAutomaton generalizedBuchiAutomaton)
        {
            var statesByCopyNumber = GetNewStatesByCopyNumber(generalizedBuchiAutomaton);

            var newStates = statesByCopyNumber
                .Values
                .Select(x => x.Values)
                .SelectMany(x=>x);

            var finitState = statesByCopyNumber[1]
                .Where(x => generalizedBuchiAutomaton.FinalStates.First().Contains(x.Key))
                .Select(x => x.Value);

            var initStates = GetNewInitStates(generalizedBuchiAutomaton, statesByCopyNumber);

            return new BuchiAutomaton.BuchiAutomaton(
                states: newStates.ToArray(),
                initStates: initStates,
                alphabet: generalizedBuchiAutomaton.Alphabet,
                finalStates: finitState.ToArray(),
                stateMarker: GetNewStateMarker(generalizedBuchiAutomaton, statesByCopyNumber),
                edgeMarker: GetNewEdgeMarker(generalizedBuchiAutomaton, statesByCopyNumber));
        }

        #region build BuchiAutomaton from GeneralizedBuchiAutomaton

        /// <summary>
        /// int - copy number
        /// IDictionary<IState, IState> - first state is state from old state,
        /// second state is copy of first state
        /// </summary>
        private static IDictionary<int, IDictionary<IState, IState>> GetNewStatesByCopyNumber(
            IGeneralizedBuchiAutomaton generalizedBuchiAutomaton)
        {
            var result = new Dictionary<int, IDictionary<IState, IState>>();

            for (var i = 1; i <= generalizedBuchiAutomaton.FinalStates.Count; i++)
            {
                var states = new Dictionary<IState,IState>();
                foreach (var state in generalizedBuchiAutomaton.States)
                {
                    states.Add(state, new State( $"{i}-{state.Name}"));
                }
                result.Add(i, states);
            }

            return result;
        }

        private static IReadOnlyList<IState> GetNewInitStates(
            IGeneralizedBuchiAutomaton generalizedBuchiAutomaton,
            IDictionary<int, IDictionary<IState, IState>> newStatesByCopyNumber)
        {
            var result = new List<IState>();

            var newStateByOldState = newStatesByCopyNumber[1];
            foreach (var state in generalizedBuchiAutomaton.InitStates)
            {
                result.Add(newStateByOldState[state]);
            }

            return result;
        }

        private static IDictionary<IState, IReadOnlyList<ILTLFormula>> GetNewStateMarker(
            IGeneralizedBuchiAutomaton generalizedBuchiAutomaton,
            IDictionary<int, IDictionary<IState, IState>> newStatesByCopyNumber)
        {
            var result = new Dictionary<IState, IReadOnlyList<ILTLFormula>>();

            foreach (var i in newStatesByCopyNumber)
            {
                foreach (var newStateByOldState in i.Value)
                {
                    var marks = generalizedBuchiAutomaton.StateMarker[newStateByOldState.Key];
                    result.Add(newStateByOldState.Value, marks);
                }
            }

            return result;
        }

        private static IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> GetNewEdgeMarker(
            IGeneralizedBuchiAutomaton generalizedBuchiAutomaton,
            IDictionary<int, IDictionary<IState, IState>> newStatesByCopyNumber)
        {
            var finalStates = new Dictionary<int, IReadOnlyList<IState>>();
            for (var i = 1; i <= generalizedBuchiAutomaton.FinalStates.Count; i++)
            {
                finalStates.Add(i, generalizedBuchiAutomaton.FinalStates[i - 1]);
            }

            var result = new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>();
            for (var copyNumber = 1; copyNumber <= newStatesByCopyNumber.Count; copyNumber ++)
            {
                var copy = newStatesByCopyNumber[1];
                foreach (var edgeMarker in generalizedBuchiAutomaton.EdgeMarker)
                {
                    if (finalStates[copyNumber].Contains(edgeMarker.Key.FirstState))
                    {
                        if (copyNumber == newStatesByCopyNumber.Count())
                        {
                            var nextCopy = newStatesByCopyNumber[1];
                            result.Add(
                                new TotalRatio(copy[edgeMarker.Key.FirstState], nextCopy[edgeMarker.Key.SecondState]),
                                edgeMarker.Value);
                        }
                        else
                        {
                            var nextCopy = newStatesByCopyNumber[copyNumber + 1];
                            result.Add(
                                new TotalRatio(copy[edgeMarker.Key.FirstState], nextCopy[edgeMarker.Key.SecondState]),
                                edgeMarker.Value);
                        }
                    }
                    else
                    {
                        result.Add(
                            new TotalRatio(copy[edgeMarker.Key.FirstState], copy[edgeMarker.Key.SecondState]),
                            edgeMarker.Value);
                    }
                }
            }

            return result;
        }

        #endregion build BuchiAutomaton from GeneralizedBuchiAutomaton

        #region private methods

        private static IDictionary<IState, IReadOnlyList<ILTLFormula>> BuildState(ILTLFormula formula)
        {
            var result = new Dictionary<IState, IReadOnlyList<ILTLFormula>>();

            var atoms = AtomsBuilder.Build(formula);
            var i = 0;
            foreach (var atom in atoms)
            {
                result.Add(new State($"S{i}"), atom);
                i++;
            }

            return result;
        }

        private static IReadOnlyList<IReadOnlyList<ILTLFormula>> BuildAlphabet(ILTLFormula formula)
        {
            var allAtomicPredicate = AlgorithmsHelper.GetAllSubFormulas(formula)
                .Where(x => x.Type == LTLFormulaType.AtomicPredicate)
                .ToArray();

            return AlgorithmsHelper.GetAllSubFormulasSubSets(allAtomicPredicate);
        }

        //private static IDictionary<ITotalRatio, IReadOnlyList<IReadOnlyList<ILTLFormula>>> BuildEdgeMarker(
        //    IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
        //    IReadOnlyList<IReadOnlyList<ILTLFormula>> setOfAtomicPredicatesSuibSets,
        //    IReadOnlyList<ILTLFormula> allSubFormulas)
        //{
        //    var result = new Dictionary<ITotalRatio, IReadOnlyList<IReadOnlyList<ILTLFormula>>>();

        //    foreach (var state1 in atomByState.Keys)
        //    {
        //        foreach (var state2 in atomByState.Keys)
        //        {
        //            var atom1 = atomByState[state1];
        //            var atom2 = atomByState[state2];

        //            if (!AllRulsAreFeasibly(atom1, atom2, allSubFormulas))
        //            {
        //                continue;
        //            }

        //            var ratio = new TotalRatio(state1, state2);
        //            var marker = new List<IReadOnlyList<ILTLFormula>>();
        //            if (atom1.FirstOrDefault(x => x.Type == LTLFormulaType.AtomicPredicate) == null)
        //            {
        //                marker.Add(new List<ILTLFormula>());
        //            }
        //            else
        //            {
        //                foreach (var predicaes in setOfAtomicPredicatesSuibSets)
        //                {
        //                    if (predicaes.Any() && predicaes.All(x => atom1.Contains(x)))
        //                    {
        //                        marker.Add(predicaes);
        //                    }
        //                }
        //            }

        //            result.Add(ratio, marker);
        //        }
        //    }

        //    return result;
        //}

        private static IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> BuildEdgeMarker(
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IReadOnlyList<IReadOnlyList<ILTLFormula>> setOfAtomicPredicatesSuibSets,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var result = new Dictionary<ITotalRatio, IReadOnlyList<ILTLFormula>>();

            foreach (var state1 in atomByState.Keys)
            {
                foreach (var state2 in atomByState.Keys)
                {
                    var atom1 = atomByState[state1];
                    var atom2 = atomByState[state2];

                    if (!AllRulsAreFeasibly(atom1, atom2, allSubFormulas))
                    {
                        continue;
                    }

                    var ratio = new TotalRatio(state1, state2);
                    var marker = new List<ILTLFormula>();
                    if (atom1.FirstOrDefault(x => x.Type == LTLFormulaType.AtomicPredicate) == null)
                    {
                        marker.Add(new Empty()); //TODO: mat be create new class for empty set?
                    }
                    else
                    {
                        foreach (var predicaes in setOfAtomicPredicatesSuibSets)
                        {
                            if (predicaes.Any() && predicaes.All(x => atom1.Contains(x)))
                            {
                                marker = predicaes.ToList();
                            }
                        }
                    }

                    result.Add(ratio, marker);
                }
            }

            return result;
        }

        private static bool AllRulsAreFeasibly(
            IReadOnlyList<ILTLFormula> atom1,
            IReadOnlyList<ILTLFormula> atom2,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            if (allSubFormulas.Any(x => x.Type == LTLFormulaType.X))
            {
                if (!EngagementRules.RuleC1IsFeasibly(atom1, atom2, allSubFormulas))
                {
                    return false;
                }
            }

            if (allSubFormulas.Any(x => x.Type == LTLFormulaType.U))
            {
                if (!EngagementRules.RuleC2IsFeasibly(atom1, atom2, allSubFormulas))
                {
                    return false;
                }
            }

            if (allSubFormulas.Any(x => x.Type == LTLFormulaType.F))
            {
                if (!EngagementRules.RuleC3IsFeasibly(atom1, atom2, allSubFormulas))
                {
                    return false;
                }
            }

            if (allSubFormulas.Any(x => x.Type == LTLFormulaType.G))
            {
                if (!EngagementRules.RuleC4IsFeasibly(atom1, atom2, allSubFormulas))
                {
                    return false;
                }
            }

            return true;
        }

        private static IReadOnlyList<IReadOnlyList<IState>> BuildFinalStates(
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IReadOnlyList<ILTLFormula> allSubFormulas,
            IReadOnlyList<IState> unreachableStates)
        {
            var finalStates1 = FinalStateRules.GetFinalStatesByRuleD1(atomByState, allSubFormulas);
            var finalStates2 = FinalStateRules.GetFinalStatesByRuleD2(atomByState, allSubFormulas);
            var finalStates3 = FinalStateRules.GetFinalStatesByRuleD3(atomByState, allSubFormulas);

            /*if (!finalStates1.Any() && !finalStates2.Any() && finalStates3.Any())
            {
                return finalStates3.Except(unreachableStates).ToArray();
            }
            
            if (finalStates1.Any() && !finalStates2.Any() && !finalStates3.Any())
            {
                return finalStates1.Except(unreachableStates).ToArray();
            }

            if (!finalStates1.Any() && finalStates2.Any() && !finalStates3.Any())
            {
                return finalStates2.Except(unreachableStates).ToArray();
            }

            if (finalStates1.Any() && finalStates2.Any() && !finalStates3.Any())
            {
                return finalStates1.Union(finalStates2).ToArray().Intersect(unreachableStates).ToArray();
            }

            if (finalStates1.Any() && !finalStates2.Any() && finalStates3.Any())
            {
                return finalStates1.Union(finalStates3).ToArray().Intersect(unreachableStates).ToArray();
            }

            if (!finalStates1.Any() && finalStates2.Any() && finalStates3.Any())
            {
                var s = finalStates2.Intersect(finalStates3).ToArray();
                var a1 = s.Except(unreachableStates).ToArray();
                return a1;
            }

            var finalState =  finalStates1
                .Intersect(finalStates2)
                .Intersect(finalStates3)
                .ToArray();

            return finalState.Except(unreachableStates).ToArray();*/

            return finalStates1
                .Union(finalStates2)
                .Union(finalStates3)
                .ToArray();
        }

        private static IReadOnlyList<IState> GetUnreachableStates(
            IReadOnlyList<IState> initStates,
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> edgeMarker)
        {
            var result = new List<IState>();

            var otherStates = atomByState.Keys.Where(x => !initStates.Contains(x)).ToArray();

            if(otherStates == null)
            {
                return null;
            }

            foreach (var otherState in otherStates)
            {
              if (initStates.All(x => !StateIsUnreachableToOtherState(x, otherState, atomByState.Keys.ToArray(), edgeMarker)))
                {
                    result.Add(otherState);
                }
            }

            return result;
        }

        private static bool StateIsUnreachableToOtherState(
            IState state1,
            IState state2,
            IReadOnlyList<IState> allStates,
            IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> edgeMarker)
        {
            var markedState = new Dictionary<IState, bool>();
            foreach (var state in allStates)
            {
                markedState.Add(state, false);
            }
            markedState[state1] = true;

            var parentState = state1;
            var startState = state1;
            var allChidrenOfStartStateWereMarked = false;

            while (!markedState[state2] && !allChidrenOfStartStateWereMarked)
            {
                var nextState = edgeMarker
                    .FirstOrDefault(x => x.Key.FirstState == startState && !markedState[x.Key.SecondState]
                                        && x.Key.SecondState != parentState && x.Key.SecondState != startState);

                if (nextState.Key == null)
                {
                    if (startState == state1)
                    {
                        allChidrenOfStartStateWereMarked = true;
                        continue;
                    }

                    markedState[startState] = true;
                    startState = parentState;
                    continue;
                }

                parentState = startState;
                startState = nextState.Key.SecondState;
            }

            if (!markedState[state2])
            {
                return false;
            }
            return true;
        }

        #endregion private methods
    }
}