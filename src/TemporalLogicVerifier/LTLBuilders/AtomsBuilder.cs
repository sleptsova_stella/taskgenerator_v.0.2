﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Common;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLBuilders
{
    public static class AtomsBuilder
    {
        public static IReadOnlyList<IReadOnlyList<ILTLFormula>> Build(ILTLFormula formula)
        {
            if (formula == null)
            {
                return null;
            }

            var possibleAtoms = AlgorithmsHelper.GetAllPossibleAtoms(formula);
            var allSubFormulas = AlgorithmsHelper.GetAllSubFormulas(formula);
            var checkedRules = GetCheckedRules(allSubFormulas);

            var resultAtoms = new List<IReadOnlyList<ILTLFormula>>();
            foreach (var atom in possibleAtoms)
            {
                if (atom != null && atom.Count() == 0)
                {
                    resultAtoms.Add(atom);
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R1) && !RuleR1IsFeasibly(atom))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R2) && !RuleR2IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R3) && !RuleR3IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R4) && !RuleR4IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R5) && !RuleR5IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R6) && !RuleR6IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R7) && !RuleR7IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                if (checkedRules.Contains(AtomsRule.R8) && !RuleR8IsFeasibly(atom, allSubFormulas))
                {
                    continue;
                }

                resultAtoms.Add(atom);
            }

            return resultAtoms;
        }

        private enum AtomsRule
        {
            R1, R2, R3, R4, R5, R6, R7, R8
        }

        private static  IReadOnlyList<AtomsRule> GetCheckedRules(IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var verifiableFormulas = new List<AtomsRule>()
            {
                AtomsRule.R1
            };

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.AtomicPredicate) == null)
            {
                throw new Exception("ERROR - GetCheckedRules: it isn't possible");
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.Or) != null)
            {
                verifiableFormulas.Add(AtomsRule.R2);
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.U) != null)
            {
                verifiableFormulas.Add(AtomsRule.R3);
                verifiableFormulas.Add(AtomsRule.R4);
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.And) != null)
            {
                verifiableFormulas.Add(AtomsRule.R5);
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.Implication) != null)
            {
                verifiableFormulas.Add(AtomsRule.R5);
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.F) != null)
            {
                verifiableFormulas.Add(AtomsRule.R7);
            }

            if (allSubFormulas.FirstOrDefault(x => x.Type == LTLFormulaType.G) != null)
            {
                verifiableFormulas.Add(AtomsRule.R8);
            }

            return verifiableFormulas;
        }

        //TODO: in rules:  =>, <=>
        /// <summary>
        /// Check feasibility rule R1 for this atom: \fi \in A  <==> (\not \fi) (\not \in) A
        /// </summary>
        private static bool RuleR1IsFeasibly(IReadOnlyList<ILTLFormula> atoms)
        {
            if (atoms == null || !atoms.Any())
            {
                return false;
            }

            var allNegativeFormulas = GetAllFormulasByType(atoms, LTLFormulaType.NegativeFormula);

            if (allNegativeFormulas==null || !allNegativeFormulas.Any())
            {
                return true;
            }
            var allSubNegFormulas = allNegativeFormulas.Select(x => x.Left);

            foreach (var f in atoms)
            {
                if (allSubNegFormulas.Contains(f))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R2 for this atom: (f1 \/ f2) in A  <==> (f1 in A) or (f2 in A)
        /// </summary>
        private static bool RuleR2IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            if (atoms == null || !atoms.Any())
            {
                return false;
            }

            var allOrFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.Or);

            if (allOrFormulas == null || !allOrFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleR2IsFeasibly: set of formula doesn't contain Or");
            }


            foreach (var or in allOrFormulas)
            {
                if ((atoms.Contains(or) && !atoms.Contains(or.Left) && !atoms.Contains(or.Right))
                    || (!atoms.Contains(or) && (atoms.Contains(or.Left) || atoms.Contains(or.Right))))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R3 for this atom: ((f1 U f2) in A) and (f2 not in A) ==> (f1 in A) 
        /// </summary>
        private static bool RuleR3IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            if (atoms == null || !atoms.Any())
            {
                return false;
            }

            var allUFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.U);
            if (allUFormulas == null || !allUFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleR3IsFeasibly: set of formula doesn't contain U");
            }

            foreach (var u in allUFormulas)
            {
                if (atoms.Contains(u) && !atoms.Contains(u.Right) && !atoms.Contains(u.Left))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R4 for this atom: (f2 in A) ==> (f1 U f2) in A 
        /// </summary>
        private static bool RuleR4IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allUFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.U);
            if (allUFormulas == null || !allUFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleR4IsFeasibly: set of formula doesn't contain U");
            }

            foreach (var u in allUFormulas)
            {
                if (atoms.Contains(u.Right) && !atoms.Contains(u))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R1 for this atom: (f2 in A) ==> (f1 U f2) in A 
        /// </summary>
        private static bool RuleR5IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allAndFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.And);

            if (allAndFormulas == null || !allAndFormulas.Any())
            {
                //throw new Exception("ERROR - RuleR5IsFeasibly: set of formula doesn't contain AND");
            }

            foreach (var and in allAndFormulas)
            {
                if ((atoms.Contains(and) && (!atoms.Contains(and.Left) || !atoms.Contains(and.Right)))
                    || (!atoms.Contains(and) && atoms.Contains(and.Left) && atoms.Contains(and.Right)))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R6 for this atom: (f1=>f2 in A) ==> (f1 not in A) or (f2 in A)
        /// </summary>
        private static bool RuleR6IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allImplicationFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.Implication);
            if (allImplicationFormulas == null || !allImplicationFormulas.Any())
            {
                throw new Exception("ERROR - RuleR6IsFeasibly: set of formula doesn't contain IMPLICATION");
            }

            foreach (var i in allImplicationFormulas)
            {
                if ((atoms.Contains(i) && atoms.Contains(i.Left) && !atoms.Contains(i.Right))
                    || (!atoms.Contains(i) && (!atoms.Contains(i.Left) || atoms.Contains(i.Right))))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R7 for this atom: (f1=>f2 in A) ==> (f1 not in A) or (f2 in A)
        /// </summary>
        private static bool RuleR7IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {            
            var allFFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.F);

            if (allFFormulas == null || !allFFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleR7IsFeasibly: set of formula doesn't contain F");
            }

            foreach (var f in allFFormulas)
            {
                if (atoms.Contains(f))
                {
                    continue;
                }
                else if(atoms.Contains(f.Left))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check feasibility rule R7 for this atom: (f1=>f2 in A) ==> (f1 not in A) or (f2 in A)
        /// </summary>
        private static bool RuleR8IsFeasibly(IReadOnlyList<ILTLFormula> atoms, IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var allGFormulas = GetAllFormulasByType(allSubFormulas, LTLFormulaType.G);

            if (allGFormulas==null || !allGFormulas.Any())
            {
                //return true;
                throw new Exception("ERROR - RuleR8IsFeasibly: set of formula doesn't contain G");
            }

            foreach (var g in allGFormulas)
            {
                if (atoms.Contains(g) && !atoms.Contains(g.Left))
                {
                    return false;
                }
            }

            return true;
        }

        private static IReadOnlyList<ILTLFormula> GetAllFormulasByType(IReadOnlyList<ILTLFormula> atoms, LTLFormulaType type)
        {
            return atoms.Where(x => x.Type == type).ToList();
        }
    }
}
