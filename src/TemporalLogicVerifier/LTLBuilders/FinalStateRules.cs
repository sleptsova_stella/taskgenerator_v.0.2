﻿using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLBuilders
{
    public static class FinalStateRules
    {
        public static IReadOnlyList<IReadOnlyList<IState>> GetFinalStatesByRuleD1(
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var result = new List<IReadOnlyList<IState>>();
            var allU = allSubFormulas.Where(x => x.Type == LTLFormulaType.U);

            foreach (var u in allU)
            {
                var finalStates = new List<IState>();
                foreach (var atomAndState in atomByState)
                {
                    if(!atomAndState.Value.Contains(u) || atomAndState.Value.Contains(u.Right))
                    {
                        /*if (result.Contains(atomAndState.Key))
                        {
                            throw new Exception("ERROR - GetFinalStatesByRuleD1: It cann't be (may be?) ");
                        }*/
                        finalStates.Add(atomAndState.Key);
                    }
                }
                result.Add(finalStates);
            }

            return result;
        }

        public static IReadOnlyList<IReadOnlyList<IState>> GetFinalStatesByRuleD2(
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var result = new List<IReadOnlyList<IState>>();
            var allF = allSubFormulas.Where(x => x.Type == LTLFormulaType.F);

            foreach (var f in allF)
            {
                var finalStates = new List<IState>();
                foreach (var atomAndState in atomByState)
                {
                    if (!atomAndState.Value.Contains(f) || atomAndState.Value.Contains(f.Left))
                    {
                        /*if (result.Contains(atomAndState.Key))
                        {
                            throw new Exception("ERROR - GetFinalStatesByRuleD2: It cann't be (may be?) ");
                        }*/
                        finalStates.Add(atomAndState.Key);
                    }
                }
                result.Add(finalStates);
            }

            return result;
        }

        public static IReadOnlyList<IReadOnlyList<IState>> GetFinalStatesByRuleD3(
            IDictionary<IState, IReadOnlyList<ILTLFormula>> atomByState,
            IReadOnlyList<ILTLFormula> allSubFormulas)
        {
            var result = new List<IReadOnlyList<IState>>();
            var allF = allSubFormulas.Where(x => x.Type == LTLFormulaType.G);

            foreach (var g in allF)
            {
                var finalStates = new List<IState>();
                foreach (var atomAndState in atomByState)
                {
                    if (atomAndState.Value.Contains(g) || !atomAndState.Value.Contains(g.Left))
                    {
                        /*if (result.Contains(atomAndState.Key))
                        {
                            throw new Exception("ERROR - GetFinalStatesByRuleD3: It cann't be (may be?) ");
                        }*/
                        result.Add(finalStates);
                    }
                }
                result.Add(finalStates);
            }

            return result;
        }
    }
}
