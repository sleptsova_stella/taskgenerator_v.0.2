﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class F : ILTLFormula
    {
        public ILTLFormula Left { get; }

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.F;

        public F(ILTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
        }
    }
}
