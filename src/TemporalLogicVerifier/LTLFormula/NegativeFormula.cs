﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class NegativeFormula : ILTLFormula
    {
        public ILTLFormula Left { get; }

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.NegativeFormula;

        public NegativeFormula(ILTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
        }
    }
}
