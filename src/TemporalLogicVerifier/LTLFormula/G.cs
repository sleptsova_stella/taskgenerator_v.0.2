﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class G : ILTLFormula
    {
        public ILTLFormula Left { get; }

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.G;

        public G(ILTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
        }
    }
}
