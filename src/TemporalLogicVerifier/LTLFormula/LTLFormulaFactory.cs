﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.CTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class LTLFormulaFactory : ILTLFormulaFactory
    {
        public ILTLFormula CreateAtomicPredicate(string name)
        {
            return new AtomicPredicate(name);
        }

        public ILTLFormula CreateNegativeFormula(ILTLFormula subFormula)
        {
            return new NegativeFormula(subFormula);
        }

        public ILTLFormula CreateOr(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula)
        {
            return new Or(leftSubFormula, rightSubFormula);
        }

        public ILTLFormula CreateAnd(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula)
        {
            return new And(leftSubFormula, rightSubFormula);
        }

        public ILTLFormula CreateF(ILTLFormula subFormula)
        {
            return new F(subFormula);
        }

        public ILTLFormula CreateG(ILTLFormula subFormula)
        {
            return new G(subFormula);
        }

        public ILTLFormula CreateImplication(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula)
        {
            return new Implication(leftSubFormula,rightSubFormula);
        }

        public ILTLFormula CreateU(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula)
        {
            return new U(leftSubFormula,rightSubFormula);
        }

        public ILTLFormula CreateX(ILTLFormula subFormula)
        {
            return new X(subFormula);
        }

        public ILTLFormula CreateRandomFormula(ILTLFormula leftFormula, ILTLFormula rightFormula)
        {
            var randomFormulaNum = (new Random()).Next() % 8;

            switch (randomFormulaNum)
            {
                case 0:
                    return CreateOr(leftFormula, rightFormula);
                case 1:
                    return CreateAnd(leftFormula,rightFormula);
                case 2:
                    return CreateF(leftFormula);
                case 3:
                    return CreateG(leftFormula);
                case 4:
                    return CreateImplication(leftFormula, rightFormula);
                case 5:
                    return CreateNegativeFormula(leftFormula);
                case 6:
                    return CreateX(leftFormula);
                case 7:
                    return CreateU(leftFormula,rightFormula);
            }

            throw new Exception($"Unsupported CTL formula number - {randomFormulaNum}");
        }
    }
}