﻿using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class Empty : ILTLFormula
    {
        public ILTLFormula Left => null;

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.Empty;
    }
}
