﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class AtomicPredicate : ILTLFormula
    {
        public string Name { get; }

        public ILTLFormula Left => null;

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.AtomicPredicate;

        public AtomicPredicate(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            Name = name;
        }

        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
