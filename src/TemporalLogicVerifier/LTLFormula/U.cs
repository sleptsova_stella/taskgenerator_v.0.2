﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class U : ILTLFormula
    {
        public ILTLFormula Left { get; }

        public ILTLFormula Right { get; }

        public LTLFormulaType Type => LTLFormulaType.U;

        public U(ILTLFormula left, ILTLFormula right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }
    }
}
