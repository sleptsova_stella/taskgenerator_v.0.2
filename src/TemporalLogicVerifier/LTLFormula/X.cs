﻿using System;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.LTLFormula
{
    public class X : ILTLFormula
    {
        public ILTLFormula Left { get; }

        public ILTLFormula Right => null;

        public LTLFormulaType Type => LTLFormulaType.X;

        public X(ILTLFormula formula)
        {
            Left = formula ?? throw new ArgumentNullException(nameof(formula));
        }
    }
}
