﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.KripkeStructure
{
    public class KripkeStructureFactory : IKripkeStructureFactory
    {
        public ILtlKripkeStructure CreateKripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ILTLFormula> atomicPredicates,
            IDictionary<IState, IList<ILTLFormula>> markFunction)
        {
            return new LtlKripkeStructure(
                states,
                initialStates,
                totalRatio,
                atomicPredicates,
                markFunction);
        }

        public IKripkeStructure CreateKripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ICTLFormula> atomicPredicates,
            IDictionary<IState, IList<ICTLFormula>> markFunction)
        {
            return new KripkeStructure(
                states,
                initialStates,
                totalRatio,
                atomicPredicates,
                markFunction);
        }

        public IState CreateState(string name)
        {
            return new State(name);
        }

        public ITotalRatio CreteTotalRatio(IState firstState, IState secondState)
        {
            return new TotalRatio(firstState, secondState);
        }
    }
}