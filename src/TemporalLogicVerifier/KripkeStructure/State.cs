﻿using System;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.KripkeStructure
{
    public class State : IState
    {
        public string Name { get; }

        public State(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            Name = name;
        }

        public override bool Equals(object obj) => Name == ((IState)obj).Name;

        public override int GetHashCode() => HashCode.Combine(Name);
    }
}
