﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.KripkeStructure
{
    public class KripkeStructure : IKripkeStructure
    {
        private readonly IDictionary<IState, IList<ICTLFormula>> _markFunction;

        public IReadOnlyList<IState> States { get; }

        public IReadOnlyList<IState> InitialStates { get; }

        public IReadOnlyList<ITotalRatio> TotalRatio { get; }

        public IReadOnlyList<ICTLFormula> AtomicPredicates { get; }

        public KripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ICTLFormula> atomicPredicates,
            IDictionary<IState, IList<ICTLFormula>> markFunction)
        {
            States = states ?? throw new ArgumentNullException(nameof(states));
            InitialStates = initialStates ?? throw new ArgumentNullException(nameof(initialStates));
            TotalRatio = totalRatio ?? throw new ArgumentNullException(nameof(totalRatio));
            AtomicPredicates = atomicPredicates ?? throw new ArgumentNullException(nameof(atomicPredicates));
            _markFunction = markFunction ?? throw new ArgumentNullException(nameof(markFunction));

            foreach(var state in states)
            {
                if (_markFunction.Keys.Any(x => x.Name == state.Name))
                {
                    continue;
                }
                _markFunction[state] = new List<ICTLFormula>();
            }
        }

        public IList<ICTLFormula> MarkFunction(IState state)
        {
            if (!_markFunction.ContainsKey(state))
            {
                //TODO: to log as WARN
                //throw new Exception($"Mark function doesn't have state {state.Name}");

                return new List<ICTLFormula>(){};
            }

            return _markFunction[state];
        }

        public bool ModelChecker(ICTLFormula sourceFormula)
        {
            for (var i = 1; i < sourceFormula.DepthSize; i++)
            {
                FindSubformula(sourceFormula, i);
            }

            var finalSat = sourceFormula.Sat(this);
            foreach (var s in finalSat)
            {
                foreach (var s0 in InitialStates)
                {
                    if (s0 == s)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void FindSubformula(ICTLFormula formulaSat, int depthSize)
        {
            if (formulaSat.DepthSize > depthSize)
            {
                if (formulaSat.Left != null && !formulaSat.Marked)
                {
                    FindSubformula(formulaSat.Left, depthSize);
                }

                if (formulaSat.Right != null && !formulaSat.Marked)
                {
                    FindSubformula(formulaSat.Right, depthSize);
                }
                return;
            }

            formulaSat.Marked = true;
            var sat = formulaSat.Sat(this);
            foreach (var s in sat)
            {
                _markFunction[s].Add(formulaSat);
            }
        }
    }
}