﻿using System;
using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.BuchiAutomaton;

namespace TemporalLogicVerifier.KripkeStructure
{
    public class LtlKripkeStructure : ILtlKripkeStructure
    {
        private readonly IDictionary<IState, IList<ILTLFormula>> _markFunction;

        public IReadOnlyList<IState> States { get; }

        public IReadOnlyList<IState> InitialStates { get; }

        public IReadOnlyList<ITotalRatio> TotalRatio { get; }

        public IReadOnlyList<ILTLFormula> AtomicPredicates { get; }

        public LtlKripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ILTLFormula> atomicPredicates,
            IDictionary<IState, IList<ILTLFormula>> markFunction)
        {
            States = states ?? throw new ArgumentNullException(nameof(states));
            InitialStates = initialStates ?? throw new ArgumentNullException(nameof(initialStates));
            TotalRatio = totalRatio ?? throw new ArgumentNullException(nameof(totalRatio));
            AtomicPredicates = atomicPredicates ?? throw new ArgumentNullException(nameof(atomicPredicates));
            _markFunction = markFunction ?? throw new ArgumentNullException(nameof(markFunction));

            foreach(var state in states)
            {
                if (_markFunction.Keys.Any(x => x.Name == state.Name))
                {
                    continue;
                }
                _markFunction[state] = new List<ILTLFormula>();
            }
        }

        public IList<ILTLFormula> MarkFunction(IState state)
        {
            if (!_markFunction.ContainsKey(state))
            {
                //TODO: to log as WARN
                //throw new Exception($"Mark function doesn't have state {state.Name}");

                return new List<ILTLFormula>(){};
            }

            return _markFunction[state];
        }

        public bool ModelChecker(IBuchiAutomatonByLTLFormula ltlFormula)
        {
            throw new NotImplementedException();
        }
    }
}