﻿using System;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.KripkeStructure
{
    public class TotalRatio : ITotalRatio
    {
        public IState FirstState { get; }

        public IState SecondState { get; }

        public TotalRatio(IState firstState, IState seconfState)
        {
            FirstState = firstState ?? throw new ArgumentNullException(nameof(firstState));
            SecondState = seconfState ?? throw new ArgumentNullException(nameof(seconfState));
        }

        public bool Equals(ITotalRatio other)
        {
            return FirstState.Name == other.FirstState.Name &&
                SecondState.Name == other.SecondState.Name;
        }
    }
}
