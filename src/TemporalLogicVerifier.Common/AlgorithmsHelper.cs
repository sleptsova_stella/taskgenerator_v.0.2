﻿using System.Collections.Generic;
using System.Linq;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Common
{
    public static class AlgorithmsHelper
    {
        public static IReadOnlyList<IReadOnlyList<ILTLFormula>> GetAllSubSets(IReadOnlyList<ILTLFormula> set)
        {
            if (set == null || !set.Any())
            {
                return null;
            }

            var binary = new byte[set.Count()];
            var result = new List<List<ILTLFormula>>();

            var sizeOfAllSubSetsSet = 2 << (set.Count() - 1);
            for (var i = 0; i < sizeOfAllSubSetsSet; i++)
            {
                result.Add(GetSubSet(binary, set).ToList());
                BinaryIncrement(binary);
            }

            return result;
        }

        public static IReadOnlyList<IReadOnlyList<string>> GetAllSubSets(IReadOnlyList<string> set)
        {
            if (set == null || !set.Any())
            {
                return null;
            }

            var binary = new byte[set.Count()];
            var result = new List<List<string>>();

            var sizeOfAllSubSetsSet = 2 << (set.Count() - 1);
            for (var i = 0; i < sizeOfAllSubSetsSet; i++)
            {
                result.Add(GetSubSet(binary, set).ToList());
                BinaryIncrement(binary);
            }

            return result;
        }

        /// <summary>
        /// Get set of all subset consisting of subFormulas of ltl formula
        /// </summary>
        public static IReadOnlyList<IReadOnlyList<ILTLFormula>> GetAllPossibleAtoms(ILTLFormula formula)
        {
            if (formula == null)
            {
                return null;
            }

            var allSubFormulas = GetAllSubFormulas(formula);

            var binary = new byte[allSubFormulas.Count()];
            var result = new List<List<ILTLFormula>>();

            var sizeOfAllSubSetsSet = 2 << (allSubFormulas.Count() - 1);
            for (var i = 0; i < sizeOfAllSubSetsSet; i++)
            {
                result.Add(GetSubSet(binary, allSubFormulas).ToList());
                BinaryIncrement(binary);
            }

            return result;
        }

        public static IReadOnlyList<IReadOnlyList<ILTLFormula>> GetAllSubFormulasSubSets(IReadOnlyList<ILTLFormula> set)
        {
            if (set == null || !set.Any())
            {
                return null;
            }

            var binary = new byte[set.Count()];
            var result = new List<List<ILTLFormula>>();

            var sizeOfAllSubSetsSet = 2 << (set.Count() - 1);
            for (var i = 0; i < sizeOfAllSubSetsSet; i++)
            {
                result.Add(GetSubSet(binary, set).ToList());
                BinaryIncrement(binary);
            }

            return result;
        }

        public static IReadOnlyList<ILTLFormula> GetAllSubFormulas(ILTLFormula formula)
        {
            if (formula == null)
            {
                return null;
            }

            var result = new List<ILTLFormula>();
            AddSubFormula(formula, result);
            return result;
        }

        private static void AddSubFormula(ILTLFormula formula, IList<ILTLFormula> result)
        {
            var left = formula.Left;
            var right = formula.Right;

            if (left != null)
            {
                AddSubFormula(left, result);
            }

            if (right != null)
            {
                AddSubFormula(right, result);
            }

            if (!result.Contains(formula))
            {
                result.Add(formula);
            }
        }

        private static IReadOnlyList<T> GetSubSet<T>(byte[] binary, IReadOnlyList<T> set)
        {
            var subset = new List<T>();
            for (var i = set.Count - 1; i >= 0; i--)
            {
                if (binary[i] == 1)
                {
                    subset.Add(set[set.Count - 1 - i]);
                }
            }

            return subset;
        }

        private static byte[] BinaryIncrement(byte[] binary)
        {
            for (var i = binary.Length - 1; i >= 0; i--)
            {
                if (binary[i] == 0)
                {
                    binary[i] = 1;
                    return binary;
                }
                binary[i] = 0;
            }

            return binary;
        }
    }
}