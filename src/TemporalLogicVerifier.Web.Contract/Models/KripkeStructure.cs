﻿using System;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class KripkeStructure
    {
        public string[] States { get; }
        public string[] InitialStates { get; }
        public (string,string)[] TotalRatio { get; }
        public string[] AtomicPredicates { get; }
        public string[] MarksFunction { get; }

        public KripkeStructure(
            string[] states,
            string[] initialStates,
            (string, string)[] totalRatio,
            string[] atomicPredicates,
            string[] marksFunction)
        {
            States = states ?? throw new ArgumentNullException(nameof(states));
            InitialStates = initialStates ?? throw new ArgumentNullException(nameof(initialStates));
            TotalRatio = totalRatio ?? throw new ArgumentNullException(nameof(totalRatio));
            AtomicPredicates = atomicPredicates ?? throw new ArgumentNullException(nameof(atomicPredicates));
            MarksFunction = marksFunction ?? throw new ArgumentNullException(nameof(marksFunction));
        }
    }
}
