﻿using System.Collections.Generic;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    /// <summary>
    /// Directed graph
    /// </summary>
    public class SynchronousComposition
    {
        public IDictionary<string, int> StateNumByStateName { get; }
        public int InitState { get; }

        public IDictionary<(int, int), bool> Edges { get; }

        public SynchronousComposition(IDictionary<string, int> stateNumByStateName, int initState, IDictionary<(int, int), bool> edges)
        {
            StateNumByStateName = stateNumByStateName;
            InitState = initState;
            Edges = edges;
        }
    }
}