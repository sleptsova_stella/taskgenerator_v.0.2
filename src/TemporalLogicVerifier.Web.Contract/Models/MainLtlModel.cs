﻿using TemporalLogicVerifier.Contract.BuchiAutomaton;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class MainLtlModel
    {
        public IBuchiAutomaton LtlFormulaValue { get; }
        public IBuchiAutomaton KsValue { get; }
        public SynchronousComposition SynchronousComposition { get; }

        public BuchiAutomationByLTL LtlFormula { get; }
        public BuchiAutomationByLTL KripkeStructure { get; }

        public MainLtlModel(IBuchiAutomaton ltlFormulaValue, IBuchiAutomaton ksValue, BuchiAutomationByLTL ltlFormula,
            BuchiAutomationByLTL kripkeStructure, SynchronousComposition synchronousComposition)
        {
            LtlFormulaValue = ltlFormulaValue;
            KsValue = ksValue;
            LtlFormula = ltlFormula;
            KripkeStructure = kripkeStructure;
            SynchronousComposition = synchronousComposition;
        }
    }
}