﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class CTLFormula
    {
        public IEnumerable<CTLSubFormula> SubFormulas { get; }

        //TODO using model from other project
        public IReadOnlyList<ICTLFormula> NumericSuCtlFormulas { get; }

        public CTLFormula(IEnumerable<CTLSubFormula> subFormulas, IReadOnlyList<ICTLFormula> numericSuCtlFormulas)
        {
            SubFormulas = subFormulas;
            NumericSuCtlFormulas = numericSuCtlFormulas;
        }
    }
}