﻿namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class UserInfo
    {
        public string UserId { get; }
        public bool ModelCheckingWasRunning { get; set; }

        public UserInfo(string userId, bool modelCheckingWasRunning)
        {
            UserId = userId;
            ModelCheckingWasRunning = modelCheckingWasRunning;
        }
    }
}