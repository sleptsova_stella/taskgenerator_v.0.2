﻿using System;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class BuchiAutomationByLTL
    {
        public string[] States { get; }
        public string[] InitialStates { get; }
        public string[] FinalStates { get; }
        /// <summary>
        /// (Node1, Node2, EdgeValue)
        /// </summary>
        public (string,string,string)[] TotalRatio { get; }

        public BuchiAutomationByLTL(string[] states, string[] initialStates, string[] finalStates, (string, string, string)[] totalRatio)
        {
            States = states;
            InitialStates = initialStates;
            FinalStates = finalStates;
            TotalRatio = totalRatio;
        }
    }
}
