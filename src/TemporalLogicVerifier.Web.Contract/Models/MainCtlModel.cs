﻿using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class MainCtlModel
    {
        public ICTLFormula CtlFormulaValue { get; }
        public IKripkeStructure KripkeStructureValue { get; }
        public IState CheckingState { get; }

        public CTLFormula CtlFormula { get; }
        public KripkeStructure KripkeStructure { get; }

        public MainCtlModel
            (ICTLFormula ctlFormulaValue,
            IKripkeStructure kripkeStructureValue,
            IState checkingState,
            CTLFormula ctlFormula,
            KripkeStructure kripkeStructure)
        {
            CtlFormulaValue = ctlFormulaValue;
            KripkeStructureValue = kripkeStructureValue;
            CheckingState = checkingState;
            CtlFormula = ctlFormula;
            KripkeStructure = kripkeStructure;
        }
    }
}
