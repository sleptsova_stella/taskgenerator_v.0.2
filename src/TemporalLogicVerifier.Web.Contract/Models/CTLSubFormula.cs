﻿namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class CTLSubFormula
    {
        public string SubFormulaName { get; }
        public string SubFormulaValue { get; }

        public CTLSubFormula(
            string subFormulaName,
            string subFormulaValue)
        {
            SubFormulaName = subFormulaName;
            SubFormulaValue = subFormulaValue;
        }
    }
}
