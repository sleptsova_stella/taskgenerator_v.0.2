﻿namespace TemporalLogicVerifier.Web.Contract.Models
{
    public class VerifyModel
    {
        public string State { get; set; }
        public string[] SubFormulas { get; set; }
    }
}