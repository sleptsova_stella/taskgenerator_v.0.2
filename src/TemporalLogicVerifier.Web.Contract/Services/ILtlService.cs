﻿using System.Threading.Tasks;
using TemporalLogicVerifier.Web.Contract.Models;

namespace TemporalLogicVerifier.Web.Contract.Services
{
    public interface ILtlService
    {
        Task<MainLtlModel> GenerateMainLtlModel(string userId);

        Task<bool> Verify(string userId, string checkedStates);
    }
}