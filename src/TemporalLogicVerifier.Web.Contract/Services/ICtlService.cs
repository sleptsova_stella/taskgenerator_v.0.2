﻿using System.Threading.Tasks;
using TemporalLogicVerifier.Web.Contract.Models;

namespace TemporalLogicVerifier.Web.Contract.Services
{
    public interface ICtlService
    {
        Task<MainCtlModel> GenerateMainCtlModel(string userId);

        Task<bool> Verify(string userId, int[] formulaNumbers);
    }
}