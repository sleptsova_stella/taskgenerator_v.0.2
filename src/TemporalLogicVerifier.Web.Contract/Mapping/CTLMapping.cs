﻿using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Web.Contract.Models;

namespace TemporalLogicVerifier.Web.Contract.Mapping
{
    public static class CTLMapping
    {
        public static CTLFormula ToBl(this ICTLFormula formula)
        {
            IList<ICTLFormula> subFormulas = new List<ICTLFormula>();
            GetSubFormulas(formula, ref subFormulas);
            
            var subFormulasByName = new Dictionary<string, ICTLFormula>();

            var markedSubFormulas = new List<CTLSubFormula>();
            for (var i = 1; i <= subFormulas.Count(); i++)
            {
                var subFormulaName = $"F_{i}";
                subFormulasByName.Add(subFormulaName, subFormulas[i - 1]);

                markedSubFormulas.Add(new CTLSubFormula(
                    subFormulaName,
                    subFormulas[i - 1].ToString()));
            }
            
            return new CTLFormula(markedSubFormulas, subFormulas.ToList());
        }

        public static KripkeStructure ToBl(this IKripkeStructure kripkeStructure)
        {
            var states = kripkeStructure.States.Select(x => x.Name);
            var initStates = kripkeStructure.InitialStates.Select(x => x.Name);
            var totalRatio = kripkeStructure.TotalRatio.Select(x => (x.FirstState.Name, x.SecondState.Name));
            var predicates = kripkeStructure.AtomicPredicates.Select(x => x.ToString());
            var marksFunction = GetMarksFunction(kripkeStructure);

            return new KripkeStructure(
                states: states.ToArray(),
                initialStates: initStates.ToArray(),
                totalRatio: totalRatio.ToArray(),
                atomicPredicates: predicates.ToArray(),
                marksFunction: marksFunction.ToArray());
        }

        private static void GetSubFormulas(ICTLFormula formula, ref IList<ICTLFormula> subFormulas)
        {
            if (formula.Left != null)
            {
                GetSubFormulas(formula.Left, ref subFormulas);
            }

            if (formula.Right != null)
            {
                GetSubFormulas(formula.Right, ref subFormulas);
            }

            subFormulas.Add(formula);
        }

        private static IEnumerable<string> GetMarksFunction(IKripkeStructure kripkeStructure)
        {
            var results = new List<string>();

            foreach (var state in kripkeStructure.States)
            {
                var marks = kripkeStructure.MarkFunction(state).Select(x => x.ToString()).ToList();

                //results.Add($"State[{state.Name}] = {string.Join(",", marks)}");
                if (marks.Any())
                {
                    var markedStateValue = string.Join(", ", marks);
                    markedStateValue = markedStateValue.Trim(new char[2] {' ', ','});

                    results.Add($"{state.Name} ({markedStateValue})");
                }
                else
                {
                    results.Add($"{state.Name}");
                }
            }

            return results;
        }

        /*private static IDictionary<string, IDictionary<string, ICTLFormula>> _subFormulasStorage
            = new Dictionary<string, IDictionary<string, ICTLFormula>>();
            
        public static Api.KripkeStructure MapToContract(IKripkeStructure kripkeStructure)
        {
        }

        //todo: replace api models to Api project + mapping
        public static Tuple<IState, IReadOnlyList<ICTLFormula>> MatToContract(Api.VerifyModel verifyModel, string userId)
        {
            var formulas = _subFormulasStorage[userId]
                .Where(x => verifyModel.SubFormulas.Contains(x.Key))
                .Select(x => x.Value);

            return new Tuple<IState, IReadOnlyList<ICTLFormula>>(
                new State(verifyModel.State),
                formulas.ToList());
        }*/
    }
}
