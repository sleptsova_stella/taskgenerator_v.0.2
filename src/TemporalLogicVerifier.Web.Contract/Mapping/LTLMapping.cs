﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Web.Contract.Models;

namespace TemporalLogicVerifier.Web.Contract.Mapping
{
    public static class LTLMapping
    {
        public static BuchiAutomationByLTL ToBl(this IBuchiAutomaton buchiAutomaton)
        {
            var states = buchiAutomaton.States.Select(x => x.Name);
            var finalStates = buchiAutomaton.FinalStates.Select(x => x.Name);
            var initStates = buchiAutomaton.InitStates.Select(x => x.Name);

            var totalRatioGrouping = buchiAutomaton.EdgeMarker
                .GroupBy(x => (x.Key.FirstState.Name, x.Key.SecondState.Name));

            var totalRatio = new List<(string, string, string)>();

            foreach (var value in totalRatioGrouping)
            {
                var edgeValue = new List<string>();
                foreach (var tr in value)
                {
                    edgeValue.Add(tr.Value == null || tr.Value.Count() == 0
                        ? "{ }"
                        : tr.Value.Count() == 1 && tr.Value.Single().Type == LTLFormulaType.Empty
                            ? "{ }"
                            : "{"+ string.Join(", ", tr.Value.Select(y => y.ToString())) + "}" );
                }
                totalRatio.Add((
                    value.Key.Item1.ToString(),
                    value.Key.Item2.ToString(),
                    "{ "+ $"{ string.Join(",",edgeValue)}".TrimEnd(new []{' ', ','}) + " }"));
            }

            return new BuchiAutomationByLTL(
                states: states.ToArray(),
                initialStates: initStates.ToArray(),
                totalRatio: totalRatio.ToArray(),
                finalStates: finalStates.ToArray());
        }

        /*public static KripkeStructure ToBl(this IBuchiAutomaton kripkeStructure)
        {
            var states = kripkeStructure.States.Select(x => x.Name);
            var initStates = kripkeStructure.InitialStates.Select(x => x.Name);
            var totalRatio = kripkeStructure.TotalRatio.Select(x => (x.FirstState.Name, x.SecondState.Name));
            var predicates = kripkeStructure.AtomicPredicates.Select(x => x.ToString());
            var marksFunction = GetMarksFunction(kripkeStructure);

            return new KripkeStructure(
                states: states.ToArray(),
                initialStates: initStates.ToArray(),
                totalRatio: totalRatio.ToArray(),
                atomicPredicates: predicates.ToArray(),
                marksFunction: marksFunction.ToArray());
        }
        
        private static IEnumerable<string> GetMarksFunction(ILtlKripkeStructure kripkeStructure)
        {
            var results = new List<string>();

            foreach (var state in kripkeStructure.States)
            {
                var marks = kripkeStructure.MarkFunction(state).Select(x => x.ToString()).ToList();

                //results.Add($"State[{state.Name}] = {string.Join(",", marks)}");
                if (marks.Any())
                {
                    var markedStateValue = string.Join(", ", marks);
                    markedStateValue = markedStateValue.Trim(new char[2] {' ', ','});

                    results.Add($"{state.Name}");
                }
                else
                {
                    results.Add($"{state.Name}");
                }
            }

            return results;
        }
        */
    }
}
