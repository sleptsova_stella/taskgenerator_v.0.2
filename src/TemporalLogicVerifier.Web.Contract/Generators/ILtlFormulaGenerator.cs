﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Web.Contract.Generators
{
    public interface ILtlFormulaGenerator
    {
        //TODO web using Verifier.Contract
        ILTLFormula InitRandom();
        IReadOnlyList<ILTLFormula> GetAtomicPredicatesUsedByFormula(ILTLFormula ctlFormula);
    }
}