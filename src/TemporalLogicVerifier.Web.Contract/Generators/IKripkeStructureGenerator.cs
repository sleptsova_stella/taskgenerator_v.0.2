﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Web.Contract.Generators
{
    public interface IKripkeStructureGenerator
    {
        //TODO using Verifier.Contract
        IKripkeStructure InitRandom(IReadOnlyList<ICTLFormula> atomicPredicates);
        ILtlKripkeStructure InitRandom(IReadOnlyList<ILTLFormula> atomicPredicates);
        IState GetCheckingState(IKripkeStructure kripkeStructure);

        /// <summary>
        /// Проверить, маркировано ли заданное состояние заданными формулами
        /// после завершения алгоритма model checking
        /// </summary>
        //bool CheckMarkedsStates(string userId, IState state, IReadOnlyList<ICTLFormula> formulas);
    }
}