﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;

namespace TemporalLogicVerifier.Web.Contract.Generators
{
    public interface ICtlFormulaGenerator
    {
        //TODO web using Verifier.Contract
        ICTLFormula InitRandom();
        IReadOnlyList<ICTLFormula> GetAtomicPredicatesUsedByFormula(ICTLFormula ctlFormula);
    }
}
