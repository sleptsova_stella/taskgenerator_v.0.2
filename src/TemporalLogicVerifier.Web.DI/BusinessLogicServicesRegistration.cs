﻿using Microsoft.Extensions.Caching.Memory;
using TemporalLogicVerifier.Web.Contract.Generators;
using TemporalLogicVerifier.Web.Services;
using TemporalLogicVerifier.Web.Contract.Services;
using TemporalLogicVerifier.Web.Generators;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class BusinessLogicServicesRegistration
    {
        public static IServiceCollection AddBusinessLogicServices(this IServiceCollection services)
        {
            services.AddScoped<ICtlService, CtlService>();
            services.AddScoped<ILtlService, LtlService>();
            services.AddScoped<ICtlFormulaGenerator, CtlFormulaGenerator>();
            services.AddScoped<ILtlFormulaGenerator, LtlFormulaGenerator>();
            services.AddScoped<IKripkeStructureGenerator, KripkeStructureGenerator>();

            services.AddSingleton<IMemoryCache>(new MemoryCache(new MemoryCacheOptions()));
            return services;
        }
    }
}
