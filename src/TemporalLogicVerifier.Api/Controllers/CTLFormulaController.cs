﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TemporalLogicVerifier.Api.Models;
using TemporalLogicVerifier.Web.Contract.Models;
using TemporalLogicVerifier.Web.Contract.Services;

namespace TemporalLogicVerifier.Api.Controllers
{
    [Route("api/ctl")]
    [ApiController]
    public class CTLFormulaController : ControllerBase
    {
        //TODO: Add logs
        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ICtlService _ltlService;

        public CTLFormulaController(ICtlService ltlService)
        {
            _ltlService = ltlService ?? throw new ArgumentNullException(nameof(ltlService));
        }

        [HttpPost]
        [Route("verify/{userId}")]
        public async Task<JsonResult> Verify(string userId, [FromBody] CheckingFormulas subformulasNums)
        {
            if (string.IsNullOrEmpty(userId))
            {
                //TODO: return http code: 400? 500? 403?
                //return new HttpResponseMessage(HttpStatusCode.BadRequest,);
            }

            try
            {
                var result = await _ltlService.Verify(userId, subformulasNums.Values);
                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                //TODO log
                //_logger.Error(ex);
                Console.WriteLine(ex.Message + ex.StackTrace);
                return new JsonResult(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }

        [HttpGet]
        [Route("main_model/random/{userId}")]
        public async Task<JsonResult> InitRandomKripkeStructure(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                //TODO: return http code: 400? 500? 403?
                //return new HttpResponseMessage(HttpStatusCode.BadRequest,);
            }

            try
            {
                var mainCtlModel = await _ltlService.GenerateMainCtlModel(userId);
                //return new JsonResult(mainCtlModel);
                return new JsonResult(ToApi(mainCtlModel));
            }
            catch (Exception ex)
            {
                //TODO log
                //_logger.Error(ex);
                Console.WriteLine(ex.Message + ex.StackTrace);
                return new JsonResult(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }
        
        private static Models.MainCTLModel ToApi(MainCtlModel mainCtlModel)
        {
            var ksData = ToApi(mainCtlModel.KripkeStructure);
            var question = $"Какими подформулами будет помечено состояние {mainCtlModel.CheckingState.Name} после выполнения алгоритма Model Checking?";
            var subFormulas = ToApi(mainCtlModel.CtlFormula);
            return new Models.MainCTLModel(subFormulas, ksData, question);
        }

        private static IReadOnlyList<Formula> ToApi(Web.Contract.Models.CTLFormula ctlFormula)
        {
            //var formulaData = string.Join(" \n  \n ", formulasData);
            var formulasData = ctlFormula.SubFormulas
                .Select(x => $"{x.SubFormulaName} = {x.SubFormulaValue}")
                .ToList();
            var result = new List<Formula>();
            var num = 0;
            foreach (var subFormula in formulasData)
            {
                result.Add(new Formula(num.ToString(), subFormula));
                num++;
            }

            return result;
        }

        private static IReadOnlyList<KripkeStructureItem> ToApi(Web.Contract.Models.KripkeStructure ks)
        {
            var data = new List<KripkeStructureItem>();
            for (var i = 0; i < ks.States.Length; i++)
            {
                var color = "#8de761";

                if (ks.InitialStates.Any(x => x == ks.States[i]))
                {
                    color = "#ed2835";
                    data.Add(new KripkeStructureItem(
                        new KripkeStructureItemNode(
                            id: ks.States[i],
                            type: ks.States[i],
                            label: ks.MarksFunction[i],
                            color: color)));
                    continue;
                }

                data.Add(new KripkeStructureItem(
                    new KripkeStructureItemNodeWithoutColor(
                        id: ks.States[i],
                        type: ks.States[i],
                        label: ks.MarksFunction[i])));

            }

            for (var i = 0; i < ks.TotalRatio.Length; i++)
            {
                data.Add(new KripkeStructureItem(
                    new KripkeStructureItemEdge(
                        id: i.ToString(),
                        source: ks.TotalRatio[i].Item1,
                        target: ks.TotalRatio[i].Item2)));
            }

            return data;
        }
    }
}