﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using Models = TemporalLogicVerifier.Api.Models;
using TemporalLogicVerifier.Web.Contract.Models;
using TemporalLogicVerifier.Web.Contract.Services;

namespace TemporalLogicVerifier.Api.Controllers
{
    [Route("api/ltl")]
    [ApiController]
    public class LTLFormulaController : ControllerBase
    {
        //TODO: Add logs
        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ILtlService _ltlService;
        private static Random _rand = new Random();

        public LTLFormulaController(ILtlService ltlService)
        {
            _ltlService = ltlService ?? throw new ArgumentNullException(nameof(ltlService));
        }

        public class MyClass
        {
            public  string Values { get; }

            public MyClass(string values)
            {
                Values = values;
            }
        }

        [HttpPost]
        [Route("verify/{userId}")]
        public async Task<JsonResult> Verify(string userId,[FromBody] MyClass body)
        {
            if (string.IsNullOrEmpty(userId))
            {
                //TODO: return http code: 400? 500? 403?
                //return new HttpResponseMessage(HttpStatusCode.BadRequest,);
            }

            try
            {

                var result = await _ltlService.Verify(userId, body.Values);
                return new JsonResult(result);
            }
            catch (Exception ex)
            {
                //TODO log
                //_logger.Error(ex);
                Console.WriteLine(ex.Message + ex.StackTrace);
                return new JsonResult(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }

        [HttpGet]
        [Route("main_model/random/{userId}")]
        public async Task<JsonResult> InitRandomKripkeStructure(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                //TODO: return http code: 400? 500? 403?
                //return new HttpResponseMessage(HttpStatusCode.BadRequest,);
            }

            try
            {
                var mainLtlModel = await _ltlService.GenerateMainLtlModel(userId);
                return new JsonResult(ToApi(mainLtlModel));
            }
            catch (Exception ex)
            {
                //TODO log
                //_logger.Error(ex);
                Console.WriteLine(ex.Message + ex.StackTrace);
                return new JsonResult(new HttpResponseMessage(HttpStatusCode.InternalServerError));
            }
        }
        
        private static Models.MainLTLModel ToApi(MainLtlModel mainLtlModel)
        {
            var ksData = ToApi(mainLtlModel.KripkeStructure);
            var question = $"Приведите контрпример, если данная LTL формула не выполняется.";
            var ltlData = ToApi(mainLtlModel.LtlFormula);
            var states = GetStates(mainLtlModel.LtlFormulaValue, mainLtlModel.KsValue);
            return new Models.MainLTLModel(ksData, ltlData, question, states);
        }

        private static IReadOnlyList<Models.State> GetStates(
            IBuchiAutomaton ltl,
            IBuchiAutomaton ks)
        {
            var result = new List<Models.State>();

            var i = 1;
            foreach (var ltlState in ltl.States)
            {
                foreach (var ksState in ks.States)
                {
                    result.Add( new Models.State( i.ToString() ,$"{ksState.Name},{ltlState.Name}"));
                    i++;
                }
            }
            return result;
        }

        /*private static IReadOnlyList<Models.BuchiAutomationItem> ToApi(Web.Contract.Models.KripkeStructure ks)
        {
            var data = new List<Models.BuchiAutomationItem>();
            for (var i = 0; i < ks.States.Length; i++)
            {
                var color = "#8de761";

                if (ks.InitialStates.Any(x => x == ks.States[i]))
                {
                    color = "#ed2835";
                    data.Add(new Models.BuchiAutomationItem(
                        new Models.BuchiAutomationItemNode(
                            id: ks.States[i],
                            type: "heptagon",
                            label: ks.MarksFunction[i],
                            color: color)));
                    continue;
                }

                data.Add(new Models.BuchiAutomationItem(
                    new Models.BuchiAutomationItemNodeWithoutColor(
                        id: ks.States[i],
                        type: "heptagon",
                        label: ks.MarksFunction[i])));

            }

            for (var i = 0; i < ks.TotalRatio.Length; i++)
            {
                data.Add(new Models.BuchiAutomationItem(
                    new Models.BuchiAutomationItemEdge(
                        id: i.ToString(),
                        source: ks.TotalRatio[i].Item1,
                        target: ks.TotalRatio[i].Item2,
                        label: ks.MarksFunction))));
            }

            return data;
        }*/

        private static IReadOnlyList<Models.BuchiAutomationItem> ToApi(Web.Contract.Models.BuchiAutomationByLTL ks)
        {
            var data = new List<Models.BuchiAutomationItem>();
            for (var i = 0; i < ks.States.Length; i++)
            {
                var color = "#8de761";

                if (ks.InitialStates.Any(x => x == ks.States[i]))
                {
                    color = "#ed2835";
                    data.Add(new Models.BuchiAutomationItem(
                        new Models.BuchiAutomationItemNode(
                            id: ks.States[i],
                            label: ks.States[i],
                            type: ks.FinalStates.Contains(ks.States[i]) ? "heptagon" : "ellipse",
                            color: color)));
                    continue;
                }

                data.Add(new Models.BuchiAutomationItem(
                    new Models.BuchiAutomationItemNodeWithoutColor(
                        id: ks.States[i],
                        label: ks.States[i],
                        type: ks.FinalStates.Contains(ks.States[i]) ? "heptagon" : "ellipse")));

            }

            for (var i = 0; i < ks.TotalRatio.Length; i++)
            {
                data.Add(new Models.BuchiAutomationItem(
                    new Models.BuchiAutomationItemEdge(
                        id: "e"+i.ToString(),
                        source: ks.TotalRatio[i].Item1,
                        target: ks.TotalRatio[i].Item2,
                        label: ks.TotalRatio[i].Item3)));
            }

            return data;
        }
    }
}