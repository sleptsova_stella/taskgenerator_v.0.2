﻿namespace TemporalLogicVerifier.Api.Models
{
    public class State
    {
        public string Num { get; }
        public string Value { get; }

        public State(string num, string value)
        {
            Num = num;
            Value = value;
        }
    }
}