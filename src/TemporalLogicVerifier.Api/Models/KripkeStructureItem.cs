﻿using System.Collections.Generic;

namespace TemporalLogicVerifier.Api.Models
{
    public class KripkeStructureItem
    {
        public KripkeStructureItemDataBase data { get; }

        public KripkeStructureItem(KripkeStructureItemDataBase itemData)
        {
            data = itemData;
        }
    }
}
