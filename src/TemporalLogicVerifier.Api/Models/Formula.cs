﻿namespace TemporalLogicVerifier.Api.Models
{
    public class Formula
    {
        public string Num { get; }
        public string Value { get; }

        public Formula(string num, string value)
        {
            Num = num;
            Value = value;
        }
    }
}