﻿namespace TemporalLogicVerifier.Api.Models
{
    public class BuchiAutomationItemNode : BuchiAutomationItemDataBase
    {
        public string Id { get; }

        /// <summary>
        /// Node type
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// Node name (with marked predicates): S0 (p1,p2)
        /// </summary>
        public string Label { get; }

        public string Color { get; }

        public BuchiAutomationItemNode(string id, string type, string label, string color)
        {
            Id = id;
            Type = type;
            Label = label;
            Color = color;
        }
    }
}
