﻿namespace TemporalLogicVerifier.Api.Models
{
    public class CheckingFormulas
    {
        public int[] Values { get; }

        public CheckingFormulas(int[] values)
        {
            Values = values;
        }
    }
}