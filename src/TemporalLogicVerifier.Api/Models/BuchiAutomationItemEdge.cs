﻿namespace TemporalLogicVerifier.Api.Models
{
    public class BuchiAutomationItemEdge : BuchiAutomationItemDataBase
    {
        /// <summary>
        /// Edge name
        /// </summary>
        public string id { get; }

        /// <summary>
        /// Source node name
        /// </summary>
        public string source { get; }

        /// <summary>
        /// Target node name
        /// </summary>
        public string  target { get; }

        public string arrow => "triangle";

        public string label { get; }

        public BuchiAutomationItemEdge(string id, string source, string target, string label)
        {
            this.id = id;
            this.source = source;
            this.target = target;
            this.label = label;
        }
    }
}
