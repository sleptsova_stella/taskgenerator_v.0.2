﻿using System.Collections.Generic;

namespace TemporalLogicVerifier.Api.Models
{
    public class MainLTLModel
    {
        public IReadOnlyList<BuchiAutomationItem> BaKs { get; }
        public IReadOnlyList<BuchiAutomationItem> BaLtl { get; }
        public IReadOnlyList<State> States { get; }
        public string Question { get; }

        public MainLTLModel(IReadOnlyList<BuchiAutomationItem> baKs, IReadOnlyList<BuchiAutomationItem> baLtl, string question, IReadOnlyList<State> states)
        {
            BaKs = baKs;
            BaLtl = baLtl;
            Question = question;
            States = states;
        }
    }
}
