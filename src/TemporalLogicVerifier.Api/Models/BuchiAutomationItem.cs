﻿using System.Collections.Generic;

namespace TemporalLogicVerifier.Api.Models
{
    public class BuchiAutomationItem
    {
        public BuchiAutomationItemDataBase data { get; }

        public BuchiAutomationItem(BuchiAutomationItemDataBase itemData)
        {
            data = itemData;
        }
    }
}
