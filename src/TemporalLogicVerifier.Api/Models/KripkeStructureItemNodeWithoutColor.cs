﻿namespace TemporalLogicVerifier.Api.Models
{
    public class KripkeStructureItemNodeWithoutColor : KripkeStructureItemDataBase
    {
        public string Id { get; }

        /// <summary>
        /// Node name
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// Node name (with marked predicates): S0 (p1,p2)
        /// </summary>
        public string Label { get; }

        public KripkeStructureItemNodeWithoutColor(string id, string type, string label)
        {
            Id = id;
            Type = type;
            Label = label;
        }
    }
}
