﻿using System.Collections.Generic;

namespace TemporalLogicVerifier.Api.Models
{
    public class MainCTLModel
    {
        public IReadOnlyList<Formula> Formulas { get; }
        public IReadOnlyList<KripkeStructureItem> KS { get; }
        public string Question { get; }

        public MainCTLModel(IReadOnlyList<Formula> formulas, IReadOnlyList<KripkeStructureItem> ks, string question)
        {
            Formulas = formulas;
            KS = ks;
            Question = question;
        }
    }
}
