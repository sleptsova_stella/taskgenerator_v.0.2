﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace TemporalLogicVerifier.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        /*private const string ENVIRONMENT_KEY = "ASPNETCORE_ENVIRONMENT";

        public static void Main(string[] args)
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (Environment.GetEnvironmentVariable(ENVIRONMENT_KEY) == null)
            {
#if DEBUG
                Environment.SetEnvironmentVariable(ENVIRONMENT_KEY, "Development");
#else
                Environment.SetEnvironmentVariable(ENVIRONMENT_KEY, "Production");
#endif
            }

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable(ENVIRONMENT_KEY)}.json", true, true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();


            //TODO: logs
            //var logger = LogManager.GetCurrentClassLogger();

            try
            {
                BuildWebHost(args).Run();
            }
            catch (Exception ex)
            {
                //TODO: logs
                //logger.Error(ex);
                //LogManager.Flush();
                Console.WriteLine($"ERROR: {ex}");
                throw;
            }
            finally
            {
                //TODO: logs
                //LogManager.Flush();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
                   CreateWebHostBuilder(args)
                   .Build();

        private static IWebHostBuilder CreateWebHostBuilder(string[] args, IConfiguration configuration = null)
        {
            var builder = WebHost.CreateDefaultBuilder(args)
                                 .ConfigureLogging(x =>
                                 {
                                     if (configuration != null)
                                     {
                                         //TODO: logs
                                         //x.AddConfiguration(configuration.GetSection("Logging"));
                                     }
                                     //TODO: logs
                                     //x.ClearProviders();
                                     //x.AddNLog();
                                 })
                                 .UseStartup<Startup>();

            //TODO: it's right?
            if (configuration != null)
            {
                builder.UseConfiguration(configuration);
                builder.UseUrls(configuration.GetValue<string>("HostUrl"));
            }

            return builder;
        }*/
    }   
}
