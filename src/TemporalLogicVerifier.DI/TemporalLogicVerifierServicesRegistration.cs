﻿using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;
using TemporalLogicVerifier.CTLFormula;
using TemporalLogicVerifier.KripkeStructure;
using TemporalLogicVerifier.LTLFormula;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class TemporalLogicVerifierServicesRegistration
    {
        public static IServiceCollection AddLogicServices(this IServiceCollection services)
        {
            services.AddScoped<IKripkeStructureFactory, KripkeStructureFactory>();
            services.AddScoped<ICTLFormulaFactory, CTLFormulaFactory>();
            services.AddScoped<ILTLFormulaFactory, LTLFormulaFactory>();
            
            return services;
        }
    }
}
 