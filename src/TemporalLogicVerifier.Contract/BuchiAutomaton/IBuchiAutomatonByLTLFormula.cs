﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Contract.BuchiAutomaton
{
    public interface IBuchiAutomatonByLTLFormula
    {
        IReadOnlyList<IReadOnlyList<ILTLFormula>> Alphabet { get; }

        IReadOnlyList<IState> States { get; }

        /// <summary>
        /// Contaiins only an one state
        /// if this IBuchiAutomaton is deterministic automaton
        /// </summary>
        IReadOnlyList<IState> InitStates { get; }

        /// <summary>
        /// Множество допускающих состояний
        /// </summary>
        IReadOnlyList<IState> FinalStates { get; }

        /// <summary>
        /// Отношение переходов для недетерменированного автомата
        /// (для детерменированного - возвращает только одного состояние)
        /// </summary>
        IReadOnlyList<IReadOnlyList<ILTLFormula>> EdgeValue(ITotalRatio ratio);

        IReadOnlyList<IReadOnlyList<ILTLFormula>> EdgeValue(IState state1, IState state2);

        IReadOnlyList<ILTLFormula> StateValue(IState state);
    }
}
