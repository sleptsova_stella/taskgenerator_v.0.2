﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.KripkeStructure;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Contract.BuchiAutomaton
{
    public interface IGeneralizedBuchiAutomaton
    {
        IDictionary<IState, IReadOnlyList<ILTLFormula>> StateMarker { get; }

        IDictionary<ITotalRatio, IReadOnlyList<ILTLFormula>> EdgeMarker { get; }

        IReadOnlyList<IReadOnlyList<ILTLFormula>> Alphabet { get; }

        IReadOnlyList<IState> States { get; }

        /// <summary>
        /// Contaiins only an one state
        /// if this IBuchiAutomaton is deterministic automaton
        /// </summary>
        IReadOnlyList<IState> InitStates { get; }

        /// <summary>
        /// Отношение переходов для недетерменированного автомата
        /// (для детерменированного - возвращает только одного состояние)
        /// </summary>
        IReadOnlyList<ILTLFormula> EdgeValue(ITotalRatio ratio);

        /// <summary>
        /// Множество допускающих состояний F = {F1, F2, ..., Fk}
        /// Слово допускается обощенным автоматом Бюхи, если цепочка состояний,
        /// содержит бесконечное число состояний из каждого множества Fi
        /// </summary>
        IReadOnlyList<IReadOnlyList<IState>> FinalStates { get; }
        
        IReadOnlyList<ILTLFormula> EdgeValue(IState state1, IState state2);

        IReadOnlyList<ILTLFormula> StateValue(IState state);
    }
}
