﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.BuchiAutomaton;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Contract.KripkeStructure
{
    public interface ILtlKripkeStructure
    {
        IReadOnlyList<IState> States { get; }
        IReadOnlyList<IState> InitialStates { get; }

        /// <summary>
        /// For Any s1 \in State Exist s2 \in S such that (s1, s2) \in TotalRatio
        /// </summary>
        IReadOnlyList<ITotalRatio> TotalRatio { get; }

        /// <summary>
        /// Finite set of atomic predicates
        /// </summary>
        IReadOnlyList<ILTLFormula> AtomicPredicates { get; }

        /// <summary>
        /// To each state, this map associates the set of atomic predicates that are true in it.
        /// </summary>
        IList<ILTLFormula> MarkFunction(IState state);

        bool ModelChecker(IBuchiAutomatonByLTLFormula ltlFormula);
    }
}
