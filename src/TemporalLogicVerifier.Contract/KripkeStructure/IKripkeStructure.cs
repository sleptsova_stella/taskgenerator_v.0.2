﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;

namespace TemporalLogicVerifier.Contract.KripkeStructure
{
    public interface IKripkeStructure
    {
        IReadOnlyList<IState> States { get; }
        IReadOnlyList<IState> InitialStates { get; }

        /// <summary>
        /// For Any s1 \in State Exist s2 \in S such that (s1, s2) \in TotalRatio
        /// </summary>
        IReadOnlyList<ITotalRatio> TotalRatio { get; }

        /// <summary>
        /// Finite set of atomic predicates
        /// </summary>
        IReadOnlyList<ICTLFormula> AtomicPredicates { get; }

        /// <summary>
        /// To each state, this map associates the set of atomic predicates that are true in it.
        /// </summary>
        IList<ICTLFormula> MarkFunction(IState state);

        bool ModelChecker(ICTLFormula ctlFormula);
    }
}
