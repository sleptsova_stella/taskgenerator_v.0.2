﻿using System;

namespace TemporalLogicVerifier.Contract.KripkeStructure
{
    public interface ITotalRatio : IEquatable<ITotalRatio>
    {
        IState FirstState { get; }
        IState SecondState { get; }
    }
}
