﻿namespace TemporalLogicVerifier.Contract.KripkeStructure
{
    public interface IState
    {
        string Name { get; }
    }
}
