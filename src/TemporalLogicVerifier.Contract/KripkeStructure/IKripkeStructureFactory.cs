﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.CTLFormula;
using TemporalLogicVerifier.Contract.LTLFormula;

namespace TemporalLogicVerifier.Contract.KripkeStructure
{
    public interface IKripkeStructureFactory
    {
        ILtlKripkeStructure CreateKripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ILTLFormula> atomicPredicates,
            IDictionary<IState, IList<ILTLFormula>> markFunction);

        IKripkeStructure CreateKripkeStructure(
            IReadOnlyList<IState> states,
            IReadOnlyList<IState> initialStates,
            IReadOnlyList<ITotalRatio> totalRatio,
            IReadOnlyList<ICTLFormula> atomicPredicates,
            IDictionary<IState, IList<ICTLFormula>> markFunction);

        IState CreateState(string name);

        ITotalRatio CreteTotalRatio(IState firstState, IState secondState);
    }
}