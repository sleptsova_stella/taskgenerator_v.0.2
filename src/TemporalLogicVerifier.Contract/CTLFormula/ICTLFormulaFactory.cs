﻿namespace TemporalLogicVerifier.Contract.CTLFormula
{
    public interface ICTLFormulaFactory
    {
        ICTLFormula CreateAtomicPredicate(string name);
        ICTLFormula CreateNegativeFormula(ICTLFormula subFormula);
        ICTLFormula CreateOr(ICTLFormula leftSubFormula, ICTLFormula rightSubFormula);
        ICTLFormula CreateAF(ICTLFormula subFormula);
        ICTLFormula CreateEU(ICTLFormula leftSubFormula, ICTLFormula rightSubFormula);
        ICTLFormula CreateEX(ICTLFormula subFormula);

        ICTLFormula CreateRandomFormula(ICTLFormula leftFormula, ICTLFormula rightFormula);
    }
}