﻿namespace TemporalLogicVerifier.Contract.CTLFormula
{
    public enum CTLFormulaType
    {
        AtomicPredicate,
        NegativeFormula,
        Or,
        AF,
        EU,
        EX
    }
}
