﻿using System.Collections.Generic;
using TemporalLogicVerifier.Contract.KripkeStructure;

namespace TemporalLogicVerifier.Contract.CTLFormula
{
    //TODO: формулы состояний и формулы путей (две папки) - разделить и различные контракты 
    public interface ICTLFormula
    {
        ICTLFormula Left { get; }
        ICTLFormula Right { get; }
        int DepthSize { get; }
        bool Marked { get; set; }
        CTLFormulaType CtlFormulaType { get; }

        IReadOnlyList<IState> Sat(IKripkeStructure kripkeStructure);
    }
}
