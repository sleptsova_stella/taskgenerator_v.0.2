﻿namespace TemporalLogicVerifier.Contract.LTLFormula
{
    public interface ILTLFormulaFactory
    {
        ILTLFormula CreateAtomicPredicate(string name);
        ILTLFormula CreateNegativeFormula(ILTLFormula subFormula);
        ILTLFormula CreateOr(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula);
        ILTLFormula CreateAnd(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula);
        ILTLFormula CreateF(ILTLFormula subFormula);
        ILTLFormula CreateG(ILTLFormula subFormula);
        ILTLFormula CreateImplication(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula);
        ILTLFormula CreateU(ILTLFormula leftSubFormula, ILTLFormula rightSubFormula);
        ILTLFormula CreateX(ILTLFormula subFormula);
        ILTLFormula CreateRandomFormula(ILTLFormula leftFormula, ILTLFormula rightFormula);
    }
}