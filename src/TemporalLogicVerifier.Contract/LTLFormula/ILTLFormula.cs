﻿namespace TemporalLogicVerifier.Contract.LTLFormula
{
    //TODO: формулы состояний и формулы путей (две папки) - разделить и различные контракты 
    public interface ILTLFormula
    {
        ILTLFormula Left { get; }
        ILTLFormula Right { get; }

        LTLFormulaType Type { get; }
    }
}