﻿namespace TemporalLogicVerifier.Contract.LTLFormula
{
    public enum LTLFormulaType
    {
        Empty,
        AtomicPredicate,
        And,
        Or,
        Implication,
        NegativeFormula,
        U,
        X,
        F,
        G
    }
}
